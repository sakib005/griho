import 'package:House/style_guid/color_guid.dart';
import 'package:flutter/material.dart';

class AppBackground extends StatelessWidget {
  const AppBackground({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constrain) {
      final height = constrain.maxHeight;
      final width = constrain.maxWidth;

      return Stack(
        children: <Widget>[
          Container(
            color: backgroundColor,
          ),
          Positioned(
            left: -(height/2 - width/2),
            bottom: height * 0.25,
            child: Container(
              height: height,
              width: height,
              decoration:
              BoxDecoration(shape: BoxShape.circle, color: firstColor),
            ),
          ),
          Positioned(
            left: width * 0.15,
            top: -width * 0.7,
            child: Container(
              height: width * 1.6,
              width: width * 1.6,
              decoration:
              BoxDecoration(shape: BoxShape.circle, color: secondColor),
            ),
          ),
          Positioned(
            right: -width * 0.5,
            top: -width * .4,
            child: Container(
              height: width,
              width: width,
              decoration:
              BoxDecoration(shape: BoxShape.circle, color: thirdColor),
            ),
          )
        ],
      );
    });
  }
}
