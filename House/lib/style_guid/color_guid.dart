import 'dart:ui';
import 'package:flutter/material.dart';

final Color primaryColor = Color(0xFFFF9F59);
final Color backgroundColor = Color(0xFFE4E6F1);

final Color firstColor = Colors.white.withOpacity(.3);
final Color secondColor = Colors.white.withOpacity(.4);
final Color thirdColor = Colors.white.withOpacity(.6);

final Color button = Color(0x1C2833);

List<Color> colors = [
  Colors.blueGrey[900],
  Colors.blueGrey[900],
  Colors.blueGrey[800],
  Colors.blueGrey[800],
  Colors.blueGrey[700],
  Colors.blueGrey[500]
];