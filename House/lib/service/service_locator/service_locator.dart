
import 'package:get_it/get_it.dart';

import '../rest_service.dart';

class ServiceLocator{
  void getServiceLocator(){
    GetIt.I.registerLazySingleton(()=> RestService());
  }
}