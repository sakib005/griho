import 'dart:convert';
import 'dart:io';
import 'package:House/api/api_path.dart';
import 'package:House/model/api_response.dart';
import 'package:House/model/badge.dart';
import 'package:House/model/building.dart';
import 'package:House/model/floor.dart';
import 'package:House/model/floor_details.dart';
import 'package:House/model/house.dart';
import 'package:House/model/login_response.dart';
import 'package:House/model/manage_request.dart';
import 'package:House/model/notice.dart';
import 'package:House/model/notification_list.dart';
import 'package:House/model/privacy.dart';
import 'package:House/model/registration_body.dart';
import 'package:House/model/registration_response.dart';
import 'package:House/model/building_information.dart';
import 'package:House/model/user_profile.dart';
import 'package:House/model/user_type.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';

class RestService {
  Map<String, String> header = {
    'Content-type': 'application/json',
    'Accept': 'application/json'
  };

  Future<APIResponse<Floor>> getFloorInfo({String id, String houseid}) {
    return http
        .get(
            ApiPath.FLOOR_INFO +
                "?houseid=" +
                houseid.toString() +
                "&userid=" +
                id.toString(),
            headers: header)
        .then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<Floor>(data: Floor.fromJson(pro));
      }
      return APIResponse<Floor>(
          error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) =>
            APIResponse<Floor>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<FloorDetails>> getFloorDetails(
      {String id, String houseid, int floor}) {
    return http
        .get(
            ApiPath.FLOOR_DETAILS +
                "?userId=" +
                id.toString() +
                "&HouseId=" +
                houseid.toString() +
                "&Floor=" +
                floor.toString(),
            headers: header)
        .then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<FloorDetails>(data: FloorDetails.fromJson(pro));
      }
      return APIResponse<FloorDetails>(
          error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<FloorDetails>(
            error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<Privacy>> getPrivacy() {
    return http.get(ApiPath.PRIVACY, headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<Privacy>(data: Privacy.fromJson(pro));
      }
      return APIResponse<Privacy>(
          error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) =>
        APIResponse<Privacy>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<Notice>> getNotice({String id, String houseid, int page}) {
    return http.get(ApiPath.NOTICE +"?userid="+id+"&houseid="+houseid+"&page="+page.toString(), headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<Notice>(data: Notice.fromJson(pro));
      }
      return APIResponse<Notice>(
          error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) =>
        APIResponse<Notice>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<String>> postMessage(
      {String id, String houseid, String message, int privacyType}) {
    return http
        .get(
            ApiPath.POST_MESSAGE +
                "?userid=" +
                id.toString() +
                "&houseid=" +
                houseid.toString() +
                "&message=" +
                message +
                "&privacytype=" +
                privacyType.toString(),
            headers: header)
        .then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body)["result"];
        return APIResponse<String>(data: pro);
      }
      return APIResponse<String>(
          error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) =>
            APIResponse<String>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<String>> deletePost(
      {String userId, String houseid, String messageId}) {
    return http
        .get(ApiPath.DELETE_POST + "?userid=" + userId + "&houseid=" + houseid + "&messageid=" + messageId, headers: header)
        .then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body)["result"];
        return APIResponse<String>(data: pro);
      }
      return APIResponse<String>(
          error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) =>
        APIResponse<String>(error: true, errorMessage: err.toString()));
  }


  Future<APIResponse<String>> postReply(
      {String id, String houseid, String message, int privacyType, String parentId}) {
    return http
        .get(
        ApiPath.POST_MESSAGE +
            "?userid=" +
            id.toString() +
            "&houseid=" +
            houseid.toString() +
            "&message=" +
            message +
            "&privacytype=" +
            privacyType.toString() +
        "&parentid="+ parentId.toString(),
        headers: header)
        .then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body)["result"];
        return APIResponse<String>(data: pro);
      }
      return APIResponse<String>(
          error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) =>
        APIResponse<String>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<Badge>> getNotificationCount({String id, String houseid}) {
    return http.get(ApiPath.NOTIFICATION_BADGE +"?userid="+id.toString()+"&houseid="+houseid.toString(), headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<Badge>(data: Badge.fromJson(pro));
      }
      return APIResponse<Badge>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<Badge>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<NotificationList>> getNotificationList({String id, String houseid, int page}) {
    return http.get(ApiPath.NOTIFICATION_LIST +"?userid="+id.toString()+"&houseid="+houseid.toString()+"&page="+page.toString(), headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<NotificationList>(data: NotificationList.fromJson(pro));
      }
      return APIResponse<NotificationList>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<NotificationList>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<Notice>> getSinglePost({String userId, String houseid, String postId}) {
    return http.get(ApiPath.GET_SINGLE_POST +"?userid="+userId+"&houseid="+houseid+"&postid="+postId, headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<Notice>(data: Notice.fromJson(pro));
      }
      return APIResponse<Notice>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<Notice>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<UserType>> getUserType() {
    return http.get(ApiPath.USER_TYPE, headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<UserType>(data: UserType.fromJson(pro));
      }
      return APIResponse<UserType>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<UserType>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<RegistrationResponse>> registration({RegistrationBody body}) {
    return http.post(ApiPath.USER_REGISTRATION, body: body.toJson()).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<RegistrationResponse>(data: RegistrationResponse.fromJson(pro));
      }
      return APIResponse<RegistrationResponse>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<RegistrationResponse>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<LoginResponse>> getSignIn({String mobile, String password}) {
    return http.get(ApiPath.USER_LOGIN +"?mobileno="+mobile+"&password="+password, headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<LoginResponse>(data: LoginResponse.fromJson(pro));
      }
      return APIResponse<LoginResponse>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<LoginResponse>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<String>> verification({String mobile, String otp}) {
    return http.get(ApiPath.USER_VERIFICATION +"?mobile="+mobile+"&otp="+otp, headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body)["result"];
        return APIResponse<String>(data: pro);
      }
      return APIResponse<String>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<String>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<String>> createBuilding({Building body}) {
    var tt = body.toJson();
    return http.post(ApiPath.HOUSE_INFO, body: tt).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body)["result"];
        return APIResponse<String>(data: pro);
      }
      return APIResponse<String>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<String>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<House>> getHouseById({String houseId}) {
    return http.get(ApiPath.HOUSE_INFO_BY_ID +"?houseid=" + houseId, headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<House>(data: House.fromJson(pro));
      }
      return APIResponse<House>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<House>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<UserProfile>> getProfile({String id}) {
    return http.get(ApiPath.USER_PROFILE +"?userid=" + id, headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<UserProfile>(data: UserProfile.fromJson(pro));
      }
      return APIResponse<UserProfile>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<UserProfile>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<String>> ownerPostNotice({String userId, String houseId, String message}) {
    return http.get(ApiPath.OWNER_NOTICE_POST +"?userid="+userId+"&houseid="+houseId+"&message=" + message, headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body)["result"];
        return APIResponse<String>(data: pro);
      }
      return APIResponse<String>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<String>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<String>> updateProfile({String userId,
    String userProfession,
    String fMembers,
    String mOccupation,
    String profileImg,
    String nidFront,
    String nidBack
  }) async{
    var postUri = Uri.parse(ApiPath.UPDATE_PROFILE);
    var request = new http.MultipartRequest("POST", postUri);
    request.fields['userid'] =  userId;
    request.fields['userProfession'] =  userProfession;
    request.fields['familyMembers'] =  fMembers;
    request.fields['memberOccupation'] =  mOccupation;
    request.files.add(new http.MultipartFile.fromBytes('userimage', await File.fromUri(Uri.parse(profileImg)).readAsBytes(), filename: basename(profileImg),));
    request.files.add(new http.MultipartFile.fromBytes('nidfront', await File.fromUri(Uri.parse(nidFront)).readAsBytes(), filename: basename(nidFront),));
    request.files.add(new http.MultipartFile.fromBytes('nidback', await File.fromUri(Uri.parse(nidBack)).readAsBytes(), filename: basename(nidBack),));
    return request.send().then((res) {
      if (res.statusCode == 200) {
        var pro = res.toString();
        return APIResponse<String>(data: pro);
      }
      return APIResponse<String>(error: true, errorMessage: res.toString());
    }).catchError((err) => APIResponse<String>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<ManageRequest>> getManageList({String id, String houseId}) {
    return http.get(ApiPath.REQUEST +"?userid="+id+"&houseid="+houseId, headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<ManageRequest>(data: ManageRequest.fromJson(pro));
      }
      return APIResponse<ManageRequest>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<ManageRequest>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<String>> requestApproveOrReject({String userId, String requestId, String isApprove}) {
    return http.get(ApiPath.REQUEST_APPROVE_DISAPPROVE +
        "?requestid="+requestId+
        "&isapproved="+isApprove+
        "&userid="+userId, headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body)["result"];
        return APIResponse<String>(data: pro);
      }
      return APIResponse<String>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<String>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<String>> requestApproveOrRejectOwner({String userId, String requestId, String isApprove}) {
    return http.get(ApiPath.REQUEST_APPROVE_DISAPPROVE_OWNER +
        "?requestid="+requestId+
        "&isapproved="+isApprove+
        "&userid="+userId, headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body)["result"];
        return APIResponse<String>(data: pro);
      }
      return APIResponse<String>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<String>(error: true, errorMessage: err.toString()));
  }

  Future<APIResponse<BuildingInfo>> getHouseInformation({String houseId}) {
    return http.get(ApiPath.HOUSE_INFORMATION +
        "?houseId=$houseId", headers: header).then((res) {
      if (res.statusCode == 200) {
        var pro = json.decode(res.body);
        return APIResponse<BuildingInfo>(data: BuildingInfo.fromJson(pro));
      }
      return APIResponse<BuildingInfo>(error: true, errorMessage: json.decode(res.body)["Message"]);
    }).catchError((err) => APIResponse<BuildingInfo>(error: true, errorMessage: err.toString()));
  }

}
