import 'dart:async';

import 'package:House/pages/auth.dart';
import 'package:House/pages/dashboard.dart';
import 'package:House/pages/manage.dart';
import 'package:House/pages/notification_holder/notification.dart';
import 'package:House/pages/profile/profile_settings.dart';
import 'package:House/plugin/scale_route.dart';
import 'package:House/service/rest_service.dart';
import 'package:House/service/service_locator/service_locator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: Colors.blueGrey[900]));
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) async {
    ServiceLocator().getServiceLocator();
    runApp(Griho());
  });
}

class Griho extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Home',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: LandingPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _animation_in;

  Size size;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 4));
    _animation_in = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _controller,
        curve: Interval(0.65, 1.0, curve: Curves.elasticOut)));
    _controller.forward();
    Timer(Duration(seconds: 6), () async {
      _controller.reverse();
      Timer(Duration(milliseconds: 1400), () async {
        final prefs = await SharedPreferences.getInstance();
//        prefs.remove("userId");
        String userId = prefs.getString("userId");
        if (userId == null) {
          Navigator.push(context, ScaleRoute(page: AuthActivity()));
        } else {
          Navigator.push(
              context,
              ScaleRoute(
                  page: MyHomePage(
                title: "Home",
              )));
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.all(30),
          height: size.height,
          width: size.width,
          child: Center(
            child: ScaleTransition(
              child: Image.asset("assets/logo.png"),
              scale: _animation_in,
            ),
          ),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;
  final int sliderVal = 10;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  RestService get restService => GetIt.I<RestService>();
  bool isConnected = false;
  int _page = 0;
  GlobalKey _bottomNavigationKey = GlobalKey();

  isBottomNavShow() async{
    setState(() {
      if(isShow){
        isShow = false;
      }else{
        isShow = true;
      }
    });
  }

  var tabs = [];
  int notificationCount = 0;
  int manageCount = 0;

  String houseId, userId, userType;

  bool isShow = true;

  getUserInfo() async {
    final prefs = await SharedPreferences.getInstance();
    userId = prefs.getString('userId');
    houseId = prefs.getString('houseId');
    userType = prefs.getString('userType');

    setState(() {
      if(int.parse(userType) == 1){
        tabs = [DashboardActivity(), Manage(), NotificationActivity(),ProfileSettings(isBottomShow: isBottomNavShow,)];
      }else{
        tabs = [DashboardActivity(), NotificationActivity(),ProfileSettings(isBottomShow: isBottomNavShow,)];
      }
    });
    getNotificationCount();
  }

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays(
        [SystemUiOverlay.bottom, SystemUiOverlay.top]);

    getUserInfo();
    super.initState();
  }

  getNotificationCount() async {
    var dataList =
        await restService.getNotificationCount(id: userId, houseid: houseId);
    if (!dataList.error) {
      if (!mounted) return;
      setState(() {
        var badge = dataList.data;
        notificationCount = badge.result.notificationCount;
        manageCount = badge.result.manageCount;
        print(badge.result.notificationCount);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            iconTheme: IconThemeData(color: Colors.blueGrey[900]),
            elevation: 0,
            leading: Container(
              padding: EdgeInsets.all(10),
              child: Image.asset("assets/icon.png"),
            ),
            actions: <Widget>[
              InkWell(
                  onTap: () async {
                    final prefs = await SharedPreferences.getInstance();
                    prefs.remove('userId');
                    prefs.remove('houseId');
                    Navigator.push(context, ScaleRoute(page: AuthActivity()));
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.exit_to_app),
                  ))
            ],
          ),
          bottomNavigationBar: userType != null && isShow ? BottomNavigationBar(
            key: _bottomNavigationKey,
            type: BottomNavigationBarType.fixed,
            elevation: 5,
            items: int.parse(userType) == 1 ?[
              BottomNavigationBarItem(
                icon: _page == 0
                    ? Icon(Icons.dashboard, color: Colors.green)
                    : Icon(
                        Icons.dashboard,
                      ),
                title: _page == 0
                    ? Text(
                        "Dashboard",
                        style: TextStyle(
                            color: Colors.green,
                            fontWeight: FontWeight.bold),
                      )
                    : Text("Dashboard"),
              ),
              BottomNavigationBarItem(
                  icon: _page == 1
                      ? Stack(
                          overflow: Overflow.visible,
                          children: <Widget>[
                            manageCount != 0
                                ? Icon(Icons.bubble_chart,
                                    color: Colors.green)
                                : Align(
                                    child: Icon(Icons.bubble_chart,
                                        color: Colors.green),
                                    alignment: Alignment.topCenter,
                                  ),
                            manageCount == 0
                                ? Container()
                                : Positioned(
                                    top: -5,
                                    right: -5,
                                    child: Container(
                                      width: 18,
                                      height: 18,
                                      decoration: BoxDecoration(
                                          color: Colors.blue,
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                              color: Colors.grey[100],
                                              width: 2)),
                                      child: Center(
                                          child: Text(
                                        manageCount.toString(),
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 11),
                                      )),
                                    ),
                                  ),
                          ],
                        )
                      : Stack(
                          overflow: Overflow.visible,
                          children: <Widget>[
                            manageCount != 0
                                ? Icon(Icons.bubble_chart)
                                : Align(
                                    child: Icon(Icons.bubble_chart),
                                    alignment: Alignment.topCenter,
                                  ),
                            manageCount == 0
                                ? Container()
                                : Positioned(
                                    top: -5,
                                    right: -5,
                                    child: Container(
                                      width: 18,
                                      height: 18,
                                      decoration: BoxDecoration(
                                          color: Colors.blue,
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                              color: Colors.grey[100],
                                              width: 2)),
                                      child: Center(
                                          child: Text(
                                        manageCount.toString(),
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 11),
                                      )),
                                    ),
                                  ),
                          ],
                        ),
                  title: _page == 1
                      ? Text(
                          "Manage",
                          style: TextStyle(
                              color: Colors.green,
                              fontWeight: FontWeight.bold),
                        )
                      : Text("Manage")),
              BottomNavigationBarItem(
                  icon: _page == 2
                      ? Stack(
                          overflow: Overflow.visible,
                          children: <Widget>[
                            notificationCount != 0
                                ? Icon(Icons.notifications_active,
                                    color: Colors.green)
                                : Align(
                                    child: Icon(Icons.notifications_active,
                                        color: Colors.green),
                                    alignment: Alignment.topCenter,
                                  ),
                            notificationCount == 0
                                ? Container()
                                : Positioned(
                                    top: -5,
                                    right: -5,
                                    child: Container(
                                      width: 18,
                                      height: 18,
                                      decoration: BoxDecoration(
                                          color: Colors.blue,
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                              color: Colors.grey[100],
                                              width: 2)),
                                      child: Center(
                                          child: Text(
                                        notificationCount.toString(),
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 11),
                                      )),
                                    ),
                                  ),
                          ],
                        )
                      : Stack(
                          overflow: Overflow.visible,
                          children: <Widget>[
                            notificationCount != 0
                                ? Icon(Icons.notifications_active)
                                : Align(
                                    child: Icon(Icons.notifications_active),
                                    alignment: Alignment.topCenter,
                                  ),
                            notificationCount == 0
                                ? Container()
                                : Positioned(
                                    top: -5,
                                    right: -5,
                                    child: Container(
                                      width: 18,
                                      height: 18,
                                      decoration: BoxDecoration(
                                          color: Colors.blue,
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                              color: Colors.grey[100],
                                              width: 2)),
                                      child: Center(
                                          child: Text(
                                        notificationCount.toString(),
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 11),
                                      )),
                                    ),
                                  ),
                          ],
                        ),
                  title: _page == 2
                      ? Text(
                          "Notification",
                          style: TextStyle(
                              color: Colors.green,
                              fontWeight: FontWeight.bold),
                        )
                      : Text("Notification")),
              BottomNavigationBarItem(
                icon: _page == 3
                    ? Icon(Icons.account_circle, color: Colors.green)
                    : Icon(
                  Icons.account_circle,
                ),
                title: _page == 3
                    ? Text(
                  "Profile",
                  style: TextStyle(
                      color: Colors.green,
                      fontWeight: FontWeight.bold),
                )
                    : Text("Profile"),
              ),
            ]:
            [
              BottomNavigationBarItem(
                icon: _page == 0
                    ? Icon(Icons.dashboard, color: Colors.green)
                    : Icon(
                  Icons.dashboard,
                ),
                title: _page == 0
                    ? Text(
                  "Dashboard",
                  style: TextStyle(
                      color: Colors.green,
                      fontWeight: FontWeight.bold),
                )
                    : Text("Dashboard"),
              ),
              BottomNavigationBarItem(
                  icon: _page == 2
                      ? Stack(
                    overflow: Overflow.visible,
                    children: <Widget>[
                      notificationCount != 0
                          ? Icon(Icons.notifications_active,
                          color: Colors.green)
                          : Align(
                        child: Icon(Icons.notifications_active,
                            color: Colors.green),
                        alignment: Alignment.topCenter,
                      ),
                      notificationCount == 0
                          ? Container()
                          : Positioned(
                        top: -5,
                        right: -5,
                        child: Container(
                          width: 18,
                          height: 18,
                          decoration: BoxDecoration(
                              color: Colors.blue,
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: Colors.grey[100],
                                  width: 2)),
                          child: Center(
                              child: Text(
                                notificationCount.toString(),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 11),
                              )),
                        ),
                      ),
                    ],
                  )
                      : Stack(
                    overflow: Overflow.visible,
                    children: <Widget>[
                      notificationCount != 0
                          ? Icon(Icons.notifications_active)
                          : Align(
                        child: Icon(Icons.notifications_active),
                        alignment: Alignment.topCenter,
                      ),
                      notificationCount == 0
                          ? Container()
                          : Positioned(
                        top: -5,
                        right: -5,
                        child: Container(
                          width: 18,
                          height: 18,
                          decoration: BoxDecoration(
                              color: Colors.blue,
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: Colors.grey[100],
                                  width: 2)),
                          child: Center(
                              child: Text(
                                notificationCount.toString(),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 11),
                              )),
                        ),
                      ),
                    ],
                  ),
                  title: _page == 2
                      ? Text(
                    "Notification",
                    style: TextStyle(
                        color: Colors.green,
                        fontWeight: FontWeight.bold),
                  )
                      : Text("Notification")),
              BottomNavigationBarItem(
                icon: _page == 3
                    ? Icon(Icons.account_circle, color: Colors.green)
                    : Icon(
                  Icons.account_circle,
                ),
                title: _page == 3
                    ? Text(
                  "Profile",
                  style: TextStyle(
                      color: Colors.green,
                      fontWeight: FontWeight.bold),
                )
                    : Text("Profile"),
              ),
            ],
            onTap: (index) {
              setState(() {
                _page = index;
              });
            },
          ):
          Container(
            color: Colors.white,
            height: 0,
            width: MediaQuery.of(context).size.width,
          ),
          body: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[Color(0xFFE4E6F1), Color(0xFFE4E6F1)],
                ),
              ),
              child: tabs.length > 0 ? tabs[_page] : Container())),
    );
  }
}
