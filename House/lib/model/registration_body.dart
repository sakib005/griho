class RegistrationBody {
  String mobileno;
  String password;
  String userName;
  String usertypeid;
  String houseid;
  String sign;

  RegistrationBody(
      {this.mobileno,
        this.password,
        this.userName,
        this.usertypeid,
        this.houseid,
        this.sign});

  RegistrationBody.fromJson(Map<String, dynamic> json) {
    mobileno = json['mobileno'];
    password = json['password'];
    userName = json['userName'];
    usertypeid = json['usertypeid'];
    houseid = json['houseid'];
    sign = json['sign'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['mobileno'] = this.mobileno;
    data['password'] = this.password;
    data['userName'] = this.userName;
    data['usertypeid'] = this.usertypeid;
    data['houseid'] = this.houseid;
    data['sign'] = this.sign;
    return data;
  }
}