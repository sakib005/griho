class NotificationList {
  List<Result> result;

  NotificationList({this.result});

  NotificationList.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      result = new List<Result>();
      json['result'].forEach((v) {
        result.add(new Result.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Result {
  String userImage;
  String userName;
  String message;
  String postedTime;
  int messageId;
  String userId;
  int id;

  Result(
      {this.userImage,
        this.userName,
        this.message,
        this.postedTime,
        this.messageId,
        this.userId,
        this.id});

  Result.fromJson(Map<String, dynamic> json) {
    userImage = json['UserImage'];
    userName = json['UserName'];
    message = json['Message'];
    postedTime = json['PostedTime'];
    messageId = json['MessageId'];
    userId = json['UserId'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserImage'] = this.userImage;
    data['UserName'] = this.userName;
    data['Message'] = this.message;
    data['PostedTime'] = this.postedTime;
    data['MessageId'] = this.messageId;
    data['UserId'] = this.userId;
    data['Id'] = this.id;
    return data;
  }
}