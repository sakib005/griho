class FloorDetails {
  List<Result> result;

  FloorDetails({this.result});

  FloorDetails.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      result = new List<Result>();
      json['result'].forEach((v) {
        result.add(new Result.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Result {
  String apartmentNo;
  String userImage;
  String userName;
  String occupation;
  String mobileNo;
  int familyMembers;

  Result(
      {this.apartmentNo,
        this.userImage,
        this.userName,
        this.occupation,
        this.mobileNo,
        this.familyMembers});

  Result.fromJson(Map<String, dynamic> json) {
    apartmentNo = json['ApartmentNo'];
    userImage = json['UserImage'];
    userName = json['UserName'];
    occupation = json['Occupation'];
    mobileNo = json['MobileNo'];
    familyMembers = json['FamilyMembers'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ApartmentNo'] = this.apartmentNo;
    data['UserImage'] = this.userImage;
    data['UserName'] = this.userName;
    data['Occupation'] = this.occupation;
    data['MobileNo'] = this.mobileNo;
    data['FamilyMembers'] = this.familyMembers;
    return data;
  }
}