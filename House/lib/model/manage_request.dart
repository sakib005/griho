class ManageRequest {
  List<Result> result;

  ManageRequest({this.result});

  ManageRequest.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      result = new List<Result>();
      json['result'].forEach((v) {
        result.add(new Result.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Result {
  String apartment;
  String rent;
  String requestTime;
  String designation;
  String name;
  String familyMembers;
  String occupationOfMembers;
  String userImage;
  String nidFront;
  String nidBack;
  int houseId;
  int requestId;
  String mobile;
  int isOwner;
  String details;

  Result({this.apartment,
    this.rent,
    this.requestTime,
    this.designation,
    this.name,
    this.familyMembers,
    this.occupationOfMembers,
    this.userImage,
    this.nidFront,
    this.nidBack,
    this.houseId,
    this.requestId,
    this.mobile,
    this.isOwner,
    this.details});

  Result.fromJson(Map<String, dynamic> json) {
    apartment = json['Apartment'];
    rent = json['Rent'];
    requestTime = json['RequestTime'];
    designation = json['Designation'];
    name = json['Name'];
    familyMembers = json['FamilyMembers'];
    occupationOfMembers = json['OccupationOfMembers'];
    userImage = json['UserImage'];
    nidFront = json['NidFront'];
    nidBack = json['NidBack'];
    houseId = json['HouseId'];
    requestId = json['RequestId'];
    mobile = json['Mobile'];
    isOwner = json['IsOwner'];
    details = json['Details'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Apartment'] = this.apartment;
    data['Rent'] = this.rent;
    data['RequestTime'] = this.requestTime;
    data['Designation'] = this.designation;
    data['Name'] = this.name;
    data['FamilyMembers'] = this.familyMembers;
    data['OccupationOfMembers'] = this.occupationOfMembers;
    data['UserImage'] = this.userImage;
    data['NidFront'] = this.nidFront;
    data['NidBack'] = this.nidBack;
    data['HouseId'] = this.houseId;
    data['RequestId'] = this.requestId;
    data['Mobile'] = this.mobile;
    data['IsOwner'] = this.isOwner;
    data['Details'] = this.details;
    return data;
  }
}