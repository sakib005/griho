class RegistrationResponse {
  String result;
  int userid;
  int usertypeid;

  RegistrationResponse({this.result, this.userid, this.usertypeid});

  RegistrationResponse.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    userid = json['userid'];
    usertypeid = json['usertypeid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['userid'] = this.userid;
    data['usertypeid'] = this.usertypeid;
    return data;
  }
}