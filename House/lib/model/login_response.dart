class LoginResponse {
  String result;
  String userid;
  String usertypeid;

  LoginResponse({this.result, this.userid, this.usertypeid});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    userid = json['userid'];
    usertypeid = json['usertypeid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this.result;
    data['userid'] = this.userid;
    data['usertypeid'] = this.usertypeid;
    return data;
  }
}