class BuildingInfo {
  Result result;

  BuildingInfo({this.result});

  BuildingInfo.fromJson(Map<String, dynamic> json) {
    result =
        json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    return data;
  }
}

class Result {
  String houseId;
  String houseName;
  String totalFloor;
  String totalUnit;
  String totalOccupied;
  String totalVacant;

  Result(
      {this.houseId,
      this.houseName,
      this.totalFloor,
      this.totalUnit,
      this.totalOccupied,
      this.totalVacant});

  Result.fromJson(Map<String, dynamic> json) {
    houseId = json['HouseId'];
    houseName = json['HouseName'];
    totalFloor = json['TotalFloor'];
    totalUnit = json['TotalUnit'];
    totalOccupied = json['TotalOccupied'];
    totalVacant = json['TotalVacant'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['HouseId'] = this.houseId;
    data['HouseName'] = this.houseName;
    data['TotalFloor'] = this.totalFloor;
    data['TotalUnit'] = this.totalUnit;
    data['TotalOccupied'] = this.totalOccupied;
    data['TotalVacant'] = this.totalVacant;
    return data;
  }
}