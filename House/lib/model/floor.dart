class Floor {
  List<Info> result;

  Floor({this.result});

  Floor.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      result = new List<Info>();
      json['result'].forEach((v) {
        result.add(new Info.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Info {
  int houseId;
  int totalFloorOfBuilding;
  String houseName;
  String isSuperOwner;
  List<int> useAbleFloors;

  Info(
      {this.houseId,
      this.totalFloorOfBuilding,
      this.houseName,
      this.isSuperOwner,
      this.useAbleFloors});

  Info.fromJson(Map<String, dynamic> json) {
    houseId = json['HouseId'];
    totalFloorOfBuilding = json['TotalFloorOfBuilding'];
    houseName = json['HouseName'];
    isSuperOwner = json['IsSuperOwner'];
    useAbleFloors = json['UseAbleFloors'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['HouseId'] = this.houseId;
    data['TotalFloorOfBuilding'] = this.totalFloorOfBuilding;
    data['HouseName'] = this.houseName;
    data['IsSuperOwner'] = this.isSuperOwner;
    data['UseAbleFloors'] = this.useAbleFloors;
    return data;
  }
}