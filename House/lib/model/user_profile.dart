class UserProfile {
  Result result;

  UserProfile({this.result});

  UserProfile.fromJson(Map<String, dynamic> json) {
    result =
    json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    return data;
  }
}

class Result {
  String name;
  String mobile;
  String userImage;
  String nidFron;
  String nidBack;
  String familymembers;
  String occupationOfMembers;
  String userType;
  String designation;
  String nidNumber;

  Result(
      {this.name,
        this.mobile,
        this.userImage,
        this.nidFron,
        this.nidBack,
        this.familymembers,
        this.occupationOfMembers,
        this.userType,
        this.designation,
        this.nidNumber});

  Result.fromJson(Map<String, dynamic> json) {
    name = json['Name'];
    mobile = json['Mobile'];
    userImage = json['UserImage'];
    nidFron = json['NidFron'];
    nidBack = json['NidBack'];
    familymembers = json['Familymembers'];
    occupationOfMembers = json['OccupationOfMembers'];
    userType = json['UserType'];
    designation = json['Designation'];
    nidNumber = json['NidNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Name'] = this.name;
    data['Mobile'] = this.mobile;
    data['UserImage'] = this.userImage;
    data['NidFron'] = this.nidFron;
    data['NidBack'] = this.nidBack;
    data['Familymembers'] = this.familymembers;
    data['OccupationOfMembers'] = this.occupationOfMembers;
    data['UserType'] = this.userType;
    data['Designation'] = this.designation;
    data['NidNumber'] = this.nidNumber;
    return data;
  }
}