class Badge {
  Result result;

  Badge({this.result});

  Badge.fromJson(Map<String, dynamic> json) {
    result =
    json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    return data;
  }
}

class Result {
  int notificationCount;
  int manageCount;

  Result({this.notificationCount, this.manageCount});

  Result.fromJson(Map<String, dynamic> json) {
    notificationCount = json['notificationCount'];
    manageCount = json['manageCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['notificationCount'] = this.notificationCount;
    data['manageCount'] = this.manageCount;
    return data;
  }
}