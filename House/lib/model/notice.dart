class Notice {
  List<NoticeResult> result;

  Notice({this.result});

  Notice.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      result = new List<NoticeResult>();
      json['result'].forEach((v) {
        result.add(new NoticeResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class NoticeResult {
  String apartmentNo;
  String userImage;
  String userName;
  String message;
  String postedTime;
  String userId;
  int messageId;
  int privacyId;
  int count;
  List<ListOfSubMessages> listOfSubMessages;

  NoticeResult(
      {this.apartmentNo,
        this.userImage,
        this.userName,
        this.message,
        this.postedTime,
        this.userId,
        this.messageId,
        this.privacyId,
        this.count,
        this.listOfSubMessages});

  NoticeResult.fromJson(Map<String, dynamic> json) {
    apartmentNo = json['ApartmentNo'];
    userImage = json['UserImage'];
    userName = json['UserName'];
    message = json['Message'];
    postedTime = json['PostedTime'];
    userId = json['UserId'];
    messageId = json['MessageId'];
    privacyId = json['PrivacyId'];
    count = json['Count'];
    if (json['ListOfSubMessages'] != null) {
      listOfSubMessages = new List<ListOfSubMessages>();
      json['ListOfSubMessages'].forEach((v) {
        listOfSubMessages.add(new ListOfSubMessages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ApartmentNo'] = this.apartmentNo;
    data['UserImage'] = this.userImage;
    data['UserName'] = this.userName;
    data['Message'] = this.message;
    data['PostedTime'] = this.postedTime;
    data['UserId'] = this.userId;
    data['MessageId'] = this.messageId;
    data['PrivacyId'] = this.privacyId;
    data['Count'] = this.count;
    if (this.listOfSubMessages != null) {
      data['ListOfSubMessages'] =
          this.listOfSubMessages.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ListOfSubMessages {
  String apartmentNo;
  String userImage;
  String userName;
  String message;
  int privacyId;
  String userId;
  String postedTime;
  int messageId;

  ListOfSubMessages(
      {this.apartmentNo,
        this.userImage,
        this.userName,
        this.message,
        this.privacyId,
        this.userId,
        this.postedTime,
        this.messageId});

  ListOfSubMessages.fromJson(Map<String, dynamic> json) {
    apartmentNo = json['ApartmentNo'];
    userImage = json['UserImage'];
    userName = json['UserName'];
    message = json['Message'];
    privacyId = json['PrivacyId'];
    userId = json['UserId'];
    postedTime = json['PostedTime'];
    messageId = json['MessageId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ApartmentNo'] = this.apartmentNo;
    data['UserImage'] = this.userImage;
    data['UserName'] = this.userName;
    data['Message'] = this.message;
    data['PrivacyId'] = this.privacyId;
    data['UserId'] = this.userId;
    data['PostedTime'] = this.postedTime;
    data['MessageId'] = this.messageId;
    return data;
  }
}