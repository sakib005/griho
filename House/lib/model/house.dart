class House {
  Result result;

  House({this.result});

  House.fromJson(Map<String, dynamic> json) {
    result =
    json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    return data;
  }
}

class Result {
  int id;
  int userId;
  int houseId;
  int totalFloorOfBuilding;
  String timeStamp;
  int unit;
  String houseName;

  Result(
      {this.id,
        this.userId,
        this.houseId,
        this.totalFloorOfBuilding,
        this.timeStamp,
        this.unit,
        this.houseName});

  Result.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    userId = json['UserId'];
    houseId = json['HouseId'];
    totalFloorOfBuilding = json['TotalFloorOfBuilding'];
    timeStamp = json['TimeStamp'];
    unit = json['Unit'];
    houseName = json['HouseName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['UserId'] = this.userId;
    data['HouseId'] = this.houseId;
    data['TotalFloorOfBuilding'] = this.totalFloorOfBuilding;
    data['TimeStamp'] = this.timeStamp;
    data['Unit'] = this.unit;
    data['HouseName'] = this.houseName;
    return data;
  }
}