class Building {
  String userId;
  String houseId;
  String floor;
  String unit;
  String houseName;
  List<String> flatInfo;

  Building(
      {this.userId,
        this.houseId,
        this.floor,
        this.unit,
        this.houseName,
        this.flatInfo});

  Building.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    houseId = json['houseId'];
    floor = json['floor'];
    unit = json['unit'];
    houseName = json['houseName'];
    flatInfo = json['flatInfo'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['houseId'] = this.houseId;
    data['floor'] = this.floor;
    data['unit'] = this.unit;
    data['houseName'] = this.houseName;
    data['flatInfo'] = this.flatInfo.toString();
    return data;
  }
}