
class ApiPath{
  static const BASE_URL = "http://103.209.231.130/shl/api";
  static const FLOOR_INFO = BASE_URL + "/Floorinfo/get";
  static const FLOOR_DETAILS = BASE_URL + "/Floordetails/get";

  static const BASE = BASE_URL + "/MessagePrivacy";
  static const PRIVACY = BASE + "/GetPrivacyList";
  static const POST_MESSAGE = BASE + "/PostMessage";
  static const DELETE_POST = BASE + "/DeleteMessage";
  static const NOTICE = BASE + "/GetPostedMessages";
  static const GET_SINGLE_POST = BASE + "/GetSinglePostedMessage";

  static const NOTIFICATION_BASE = BASE_URL + "/notification";
  static const NOTIFICATION_BADGE = NOTIFICATION_BASE + "/GetNewBadgeCount";
  static const NOTIFICATION_LIST = NOTIFICATION_BASE + "/GetNotificationList";

  static const USER = BASE_URL +"/User";
  static const USER_TYPE = USER+ "/GetuserTypes";
  static const USER_REGISTRATION = USER + "/RegisterUser";
  static const USER_PROFILE = USER + "/getuserinfo";
  static const USER_LOGIN = USER + "/Login";
  static const USER_VERIFICATION = USER + "/verify";

  static const HOUSE =  BASE_URL + "/House";
  static const HOUSE_INFO = HOUSE + "/HouseInfo";
  static const HOUSE_INFO_BY_ID = HOUSE + "/HouseDetails";

  static const OWNER_NOTICE_POST = BASE + "/PostMessageowner";
  static const UPDATE_PROFILE = BASE_URL + "/Profile/SetInfo";

  static const OWNER_MANAGE = BASE_URL+ "/Owner";
  static const REQUEST =OWNER_MANAGE + "/GetFlatRequest";
  static const REQUEST_APPROVE_DISAPPROVE =OWNER_MANAGE + "/ManageFlatRequest";
  static const REQUEST_APPROVE_DISAPPROVE_OWNER =OWNER_MANAGE + "/ManageFlatRequestOwner";

  static const HOUSE_INFORMATION =BASE_URL + "/other/info";

  // http://103.209.231.130/shl/api/other/info?houseid=12345678
}