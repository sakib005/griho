import 'package:House/model/notice.dart';
import 'package:House/plugin/expansion_section.dart';
import 'package:House/plugin/mycircleavater.dart';
import 'package:House/service/rest_service.dart';
import 'package:House/style_guid/color_guid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ExpansionCard extends StatefulWidget {

  final NoticeResult notice;
  final String houseId;
  final Function onUpdate;
  ExpansionCard({Key key, this.notice, this.onUpdate, this.houseId}): super(key: key);

  @override
  _ExpansionCardState createState() => _ExpansionCardState();
}

class _ExpansionCardState extends State<ExpansionCard> {
  RestService get restService => GetIt.I<RestService>();
  bool isExpand = false;
  final GlobalKey<FormState> _kayState = GlobalKey<FormState>();
  TextEditingController _controller = new TextEditingController();

  ScrollController _scrollController;
  double boxHeight = 0.2;
  bool isClick = false;

  String houseId, userId;
  getUserInfo() async{
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      userId =  prefs.getString('userId');
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserInfo();
    _scrollController = new ScrollController(
      initialScrollOffset: 0.0,
      keepScrollOffset: true,
    );
  }
  void _toEnd() {
    _scrollController.animateTo(
      _scrollController.position.maxScrollExtent,
      duration: const Duration(milliseconds: 500),
      curve: Curves.ease,
    );
  }

  void postIssue() async {
    var formState = _kayState.currentState;
    if (formState.validate()) {
      formState.save();

      if(!isClick){
        var tt = _controller.text;
        _controller.text = "";
        final result =
        await restService.postReply(id: userId, houseid :widget.houseId, message: tt, privacyType: widget.notice.privacyId,parentId: widget.notice.messageId.toString());
        if (!result.error) {
          setState(() {
            widget.notice.listOfSubMessages.add(ListOfSubMessages(
                apartmentNo: widget.notice.apartmentNo,
                message: tt,
                messageId: 1,
                postedTime: DateTime.now().toString(),
                privacyId: widget.notice.privacyId,
                userId: userId.toString(),
                userImage: widget.notice.userImage,
                userName: widget.notice.userName
            ));
            isClick = false;
            widget.notice.count += 1;
            boxHeight = widget.notice.listOfSubMessages.length > 0 ? widget.notice.count == 1 ? 0.5 : widget.notice.count > 3 ? 0.8: 0.6 : 0.2;
            _toEnd();
            widget.onUpdate();
          });
        }
      }
    }
  }

  deletePost(messageId) async{
    var data = await restService.deletePost(userId: userId, houseid: widget.houseId, messageId: messageId);
    if(!data.error){
      widget.onUpdate();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
      margin: EdgeInsets.only(bottom: 5),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 5),
            child: Row(
              children: <Widget>[
                MyCircleAvatar(
                  radius: 20,
                  networkImage: NetworkImage(widget.notice.userImage),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(widget.notice.userName, style: TextStyle(color: Colors.black, fontSize: 12, fontWeight: FontWeight.bold),),
                    Text(new DateFormat.yMMMd().format(DateTime.parse(widget.notice.postedTime)) + " at " + new DateFormat.jm().format(DateTime.parse(widget.notice.postedTime),),
                    style: TextStyle(color: Colors.grey, fontSize: 11),)
                  ],
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.centerRight,
                      child: Container(
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            color: Colors.pink[900],
                          shape: BoxShape.circle
                        ),
                          child: Text(widget.notice.apartmentNo, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 10),))),
                ),
                InkWell(
                  onTap: (){
                    deletePost(widget.notice.messageId.toString());
                  },
                    child: Icon(Icons.more_vert)),
                SizedBox(
                  width: 10,
                )
              ],
            ),
          ),
          GestureDetector(
            onTap: (){
              setState(() {
                if(isExpand){
                  isExpand = false;
                  FocusScope.of(context).requestFocus(FocusNode());
                }else{
                  isExpand = true;
                  boxHeight = widget.notice.count > 0 ? widget.notice.count == 1 ? 0.4 : widget.notice.count > 3 ? 0.8: 0.6 : 0.2;
                  _toEnd();
                }
              });
            },
            child: Container(
              padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.grey[50],
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                children: <Widget>[
                  Text(widget.notice.message == null ? "" : widget.notice.message,
                  style: TextStyle(
                    color: Colors.grey[600]
                  ),),
                  SizedBox(
                    height: 25,
                  ),
                  Row(
                    children: <Widget>[
                      Image.asset("assets/chat.png", width: 20, color: Colors.grey[400],),
                      SizedBox(width: 10,),
                      Text(widget.notice.count.toString(), style: TextStyle(color: Colors.grey[600]),)
                    ],
                  ),
                  ExpandedSection(
                    expand: isExpand,
                    child: Container(
                      constraints: BoxConstraints(
                        maxHeight: MediaQuery.of(context).size.width * boxHeight),
                      margin: EdgeInsets.only(top: 10),
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Divider(color: Colors.grey,),
                            Expanded(child: ListView(
                              padding: EdgeInsets.only(top: 0),
                              shrinkWrap: true,
                              controller: _scrollController,
                              children: widget.notice.listOfSubMessages.map((e){
                                return userId != null && int.parse(e.userId) == int.parse(userId) ?
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, top: 5),
                                  child: Container(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          new DateFormat.jm().format(DateTime.parse(e.postedTime)),
                                          style: Theme.of(context).textTheme.body2.apply(color: Colors.grey),
                                        ),
                                        SizedBox(width: 15),
                                        Container(
                                          constraints: BoxConstraints(
                                              maxWidth: MediaQuery.of(context).size.width * .5),
                                          padding: const EdgeInsets.all(15.0),
                                          decoration: BoxDecoration(
                                            color: Colors.green[400],
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(25),
                                              bottomLeft: Radius.circular(25),
                                              topLeft: Radius.circular(25),
                                            ),
                                          ),
                                          child: Text(
                                            e.message,
                                            style: Theme.of(context).textTheme.body1.apply(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ):
                                Container(
                                  margin: EdgeInsets.only(bottom: 10, top: 5),
                                  child: Row(
                                    children: [
                                      Align(
                                        child: MyCircleAvatar(
                                          networkImage: NetworkImage(e.userImage),
                                          radius: 20,
                                        ),
                                        alignment: Alignment.topLeft,
                                      ),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            e.userName,
                                            style: Theme.of(context).textTheme.caption,
                                          ),
                                          Container(
                                            constraints: BoxConstraints(
                                                maxWidth: MediaQuery.of(context).size.width * .4),
                                            padding: const EdgeInsets.all(15.0),
                                            decoration: BoxDecoration(
                                              color: backgroundColor,
                                              borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(25),
                                                bottomLeft: Radius.circular(25),
                                                bottomRight: Radius.circular(25),
                                              ),
                                            ),
                                            child: Text(
                                              e.message,
                                              style: Theme.of(context).textTheme.body1.apply(
                                                color: Colors.black87,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(width: 15),
                                      Text(
                                          new DateFormat.jm().format(DateTime.parse(e.postedTime)),
                                        style: Theme.of(context).textTheme.body2.apply(color: Colors.grey),
                                      ),
                                    ],
                                  ),
                                );
                              }).toList(),
                            )),
                            Container(
                              padding: EdgeInsets.all(5),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(35.0),
                                        boxShadow: [
                                          BoxShadow(
                                              offset: Offset(0, 3),
                                              blurRadius: 5,
                                              color: Colors.grey)
                                        ],
                                      ),
                                      child: Row(
                                        children: [
                                          IconButton(
                                              icon: Icon(Icons.face), onPressed: () {}),
                                          Expanded(
                                            child: Form(
                                              key: _kayState,
                                              child: TextField(
                                                minLines: 1,
                                                maxLines: 3,
                                                controller: _controller,
                                                style: TextStyle(fontSize: 13),
                                                decoration: InputDecoration(
                                                  contentPadding: EdgeInsets.only(right: 10, top: 5, bottom: 5),
                                                    hintText: "Reply..",
                                                    border: InputBorder.none,),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 15),
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        postIssue();
                                      });
                                    },
                                    child: Container(
                                      padding: const EdgeInsets.all(15.0),
                                      decoration: BoxDecoration(
                                          color: Colors.green[400], shape: BoxShape.circle),
                                      child: Icon(
                                        Icons.send,
                                        color: Colors.white,
                                        size: 15,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
