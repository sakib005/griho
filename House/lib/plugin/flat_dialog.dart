import 'package:House/model/building.dart';
import 'package:House/model/house.dart';
import 'package:House/model/utils.dart';
import 'package:House/service/rest_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FlatDialog extends StatelessWidget {
  final Function updateIndex;
  final bool isDraw;
  final Function profileUpdate;
  final bool isCreate;
  final bool isAdd;
  final bool isUpdate;
  final String houseId;

  FlatDialog(
      {Key key, this.updateIndex, this.houseId, this.isCreate, this.isAdd, this.isUpdate, this.isDraw = false, this.profileUpdate})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      insetAnimationDuration: Duration(milliseconds: 200),
      child: dialogContent(context),
    );
  }

  Widget dialogContent(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: ApartmentForm(onUpdate: profileUpdate, houseId: this.houseId, isUpdate: this.isUpdate, isCreate: this.isCreate, isAdd: this.isAdd,));
  }
}

class ApartmentForm extends StatefulWidget {

  final Function onUpdate;
  final bool isCreate;
  final bool isAdd;
  final bool isUpdate;
  final String houseId;

  ApartmentForm({Key key, this.onUpdate, this.houseId, this.isCreate, this.isAdd, this.isUpdate}):super(key: key);

  @override
  _ApartmentFormState createState() => _ApartmentFormState();
}

class _ApartmentFormState extends State<ApartmentForm> {

  RestService get restService => GetIt.I<RestService>();
  final GlobalKey<FormState> _kayState = GlobalKey<FormState>();

  TextEditingController _buildingNameController = new TextEditingController();
  TextEditingController _buildingFloorController = new TextEditingController();
  TextEditingController _buildingUnitController = new TextEditingController();
  TextEditingController _buildingId = new TextEditingController();

  // Option 2
  String _selectedLocation, selectedFloor, selectedUnit, houseId = "0";
  bool isOwner = true;
  var floorList = new List<int>();
  var unitList = new List<int>();
  var apartmentList = new List<String>();
  House floor;
  bool isFullApartment = false;
  bool isPreview = false;



  String _buildingName = "", _totalFloor = "", _totalUnit = "", _houseId = "";
  final _focus = FocusNode();

  generateFloor() {
    var formState = _kayState.currentState;
    if (formState.validate()) {
      formState.save();
      setState(() {
        floorList =
            new List<int>.generate(int.parse(_totalFloor), (i) => i + 1);
      });
    }
  }

  generateUnit() {
    var formState = _kayState.currentState;
    if (formState.validate()) {
      formState.save();
      setState(() {
        unitList = new List<int>.generate(int.parse(_totalUnit), (i) => i + 1);
      });
    }
  }

  createBuilding() async{
    var formState = _kayState.currentState;
    if (formState.validate()) {
      formState.save();
      final prefs = await SharedPreferences.getInstance();
      var userId = prefs.getString('userId');
      var buliding = Building(houseId: houseId, userId: userId, houseName: _buildingName, floor: _totalFloor, unit: _totalUnit, flatInfo: apartmentList);
      var data =await restService.createBuilding(body: buliding);
      if(!data.error){
        setState(() {
          houseId = "0";
          widget.onUpdate();
          Navigator.pop(context);
        });
      }
    }
  }

  getFloorById() async{
     var data = await restService.getHouseById(houseId: _houseId);
      if(!data.error){
        setState(() {
          floor = data.data;
          houseId = floor.result.houseId.toString();
          _buildingNameController.text = floor.result.houseName;
          _buildingFloorController.text = floor.result.totalFloorOfBuilding.toString();
          _buildingUnitController.text = floor.result.unit.toString();
        });
      }
  }

  @override
  void initState() { 
    super.initState();
    if(widget.isUpdate){
      setState(() {
      _houseId = widget.houseId;
      isPreview = true;
      });
      getFloorById();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(left: 0.0, right: 0.0),
      padding: EdgeInsets.all(10),
      child: Form(
        key: _kayState,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
                padding: EdgeInsets.all(5),
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      widget.isCreate? "Create Building":
                      widget.isAdd? "Add Building":
                      "Update Building",
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.blueGrey[700]),
                    ))),
            Divider(
              height: 15,
              color: Colors.grey,
            ),
            SizedBox(height: 5),
            !isPreview || (widget.isUpdate && isPreview)?
              widget.isCreate ?
              Expanded(
              child: ListView(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                padding: EdgeInsets.all(0),
                children: <Widget>[
                  TextFormField(
                    // ignore: missing_return
                    validator: (input) {
                      if (input.isEmpty) {
                        return "Name is required";
                      }
                    },
                    controller: _buildingNameController,
                    style: TextStyle(color: Colors.black),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.text,
                    onSaved: (input) => _buildingName = input,
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).requestFocus(_focus);
                    },
                    cursorColor: Colors.blueGrey[800],
                    decoration: InputDecoration(
                      labelText: "Building Name",
                      labelStyle:
                      TextStyle(fontSize: 15, color: Colors.blueGrey[300]),
                      filled: true,
                      focusColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Colors.transparent,
                        ),
                      ),
                      prefixIcon: Icon(Icons.home, color: Colors.blueGrey[300]),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Colors.transparent,
                          width: 2.0,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  TextFormField(
                    // ignore: missing_return
                    validator: (input) {
                      if (input.isEmpty) {
                        return "Floor is required";
                      }
                    },
                    controller: _buildingFloorController,
                    style: TextStyle(color: Colors.black),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.number,
                    onSaved: (input) => _totalFloor = input,
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).requestFocus(_focus);
                    },
                    cursorColor: Colors.blueGrey[800],
                    decoration: InputDecoration(
                      labelText: "Total Floor",
                      labelStyle:
                      TextStyle(fontSize: 15, color: Colors.blueGrey[300]),
                      filled: true,
                      focusColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Colors.transparent,
                        ),
                      ),
                      prefixIcon: Icon(Icons.layers, color: Colors.blueGrey[300]),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Colors.transparent,
                          width: 2.0,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  TextFormField(
                    // ignore: missing_return
                    validator: (input) {
                      if (input.isEmpty) {
                        return "Unit is required";
                      }
                    },
                    controller: _buildingUnitController,
                    style: TextStyle(color: Colors.black),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.number,
                    onSaved: (input) => _totalUnit = input,
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).requestFocus(_focus);
                    },
                    cursorColor: Colors.blueGrey[800],
                    decoration: InputDecoration(
                      labelText: "Total Unit",
                      labelStyle:
                      TextStyle(fontSize: 15, color: Colors.blueGrey[300]),
                      filled: true,
                      focusColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Colors.transparent,
                        ),
                      ),
                      prefixIcon: Icon(Icons.pie_chart, color: Colors.blueGrey[300]),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Colors.transparent,
                          width: 2.0,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  Container(
                    color: Colors.grey[200],
                    padding: EdgeInsets.fromLTRB(20, 5, 5, 5),
                    child: Row(
                      children: <Widget>[
                        Container(
                          child: Icon(Icons.merge_type, color: Colors.blueGrey[300]),
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                        ),
                        type == null
                            ? Container()
                            : Expanded(
                          child: Container(
                            padding: EdgeInsets.only(left: 10),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                hint: Text(
                                  'Part of Apartment',
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.blueGrey[300]),
                                ),
                                value: _selectedLocation,
                                style: Theme.of(context).textTheme.title,
                                onChanged: (newValue) {
                                  if (!mounted) return;
                                  setState(() {
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                    _selectedLocation = newValue;
                                    if (newValue == "Partial") {
                                      isFullApartment = false;
                                      generateFloor();
                                    }else{
                                      floorList.clear();
                                      unitList.clear();
                                      apartmentList.clear();
                                      isFullApartment = true;
                                    }
                                  });
                                },
                                isExpanded: true,
                                items: type.map((f) {
                                  return DropdownMenuItem(
                                    child: new Text(
                                      f,
                                      style: TextStyle(
                                        color: Colors.blueGrey[800],
                                        fontSize: 15,
                                      ),
                                    ),
                                    value: f,
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 5),
                  floorList.length == 0
                      ? Container()
                      : Container(
                    color: Colors.grey[200],
                    padding: EdgeInsets.fromLTRB(20, 5, 5, 5),
                    child: Row(
                      children: <Widget>[
                        Container(
                          child: Icon(Icons.view_agenda,
                              color: Colors.blueGrey[300]),
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.only(left: 10),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                hint: Text(
                                  'Select Floor',
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.blueGrey[300]),
                                ),
                                value: selectedFloor,
                                style: Theme.of(context).textTheme.title,
                                onChanged: (newValue) {
                                  if (!mounted) return;
                                  setState(() {
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                    selectedFloor = newValue;
                                    selectedUnit = null;
                                    generateUnit();
                                  });
                                },
                                isExpanded: true,
                                items: floorList.map((f) {
                                  return DropdownMenuItem(
                                    child: new Text(
                                      f == 1
                                          ? f.toString() + "st Floor"
                                          : f == 2
                                          ? f.toString() + "nd Floor"
                                          : f == 3
                                          ? f.toString() + "ed Floor"
                                          : f.toString() + "th Floor",
                                      style: TextStyle(
                                        color: Colors.blueGrey[800],
                                        fontSize: 15,
                                      ),
                                    ),
                                    value: f.toString(),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 5),
                  unitList.length == 0
                      ? Container()
                      : Container(
                    color: Colors.grey[200],
                    padding: EdgeInsets.fromLTRB(20, 5, 5, 5),
                    child: Row(
                      children: <Widget>[
                        Container(
                          child: Icon(Icons.album,
                              color: Colors.blueGrey[300]),
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.only(left: 10),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                hint: Text(
                                  'Select Floor',
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.blueGrey[300]),
                                ),
                                value: selectedUnit,
                                style: Theme.of(context).textTheme.title,
                                onChanged: (newValue) {
                                  if (!mounted) return;
                                  setState(() {
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                    selectedUnit = newValue;
                                    print(selectedUnit);
                                    if(!apartmentList.contains(selectedUnit)){
                                      apartmentList.add(selectedUnit);
                                    }
                                  });
                                },
                                isExpanded: true,
                                items: unitList.map((f) {
                                  return DropdownMenuItem(
                                    child: new Text(
                                      selectedFloor + alphabet.elementAt(f - 1),
                                      style: TextStyle(
                                        color: Colors.blueGrey[800],
                                        fontSize: 15,
                                      ),
                                    ),
                                    value: selectedFloor + alphabet.elementAt(f - 1),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 5),
                  Center(
                    child: Wrap(
                        spacing: 10,
                        children: apartmentList.map((e){
                          return Chip(
                              backgroundColor: Colors.blueGrey,
                              label: Padding(
                                padding: EdgeInsets.all(2),
                                child: Text(e, style: TextStyle(color: Colors.white),),
                              ),
                              deleteIcon: Icon(
                                Icons.close,
                                color: Colors.white,
                              ),
                              onDeleted: () {
                                setState(() {
                                  apartmentList.remove(e);
                                });
                              });
                        }).toList()
                    ),
                  ),
                ],
              ),
            ) :
              widget.isAdd ?
                  floor == null?
                  Container(
                    child:  Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        TextFormField(
                          // ignore: missing_return
                          validator: (input) {
                            if (input.isEmpty) {
                              return "Id is required";
                            }
                          },
                          controller: _buildingId,
                          style: TextStyle(color: Colors.black),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          onSaved: (input) => _houseId = input,
                          onFieldSubmitted: (v) {
                            FocusScope.of(context).requestFocus(_focus);
                          },
                          cursorColor: Colors.blueGrey[800],
                          decoration: InputDecoration(
                            labelText: "Enter House Id",
                            labelStyle:
                            TextStyle(fontSize: 15, color: Colors.blueGrey[300]),
                            filled: true,
                            focusColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              borderSide: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                            prefixIcon: Icon(Icons.label_important, color: Colors.blueGrey[300]),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              borderSide: BorderSide(
                                color: Colors.transparent,
                                width: 2.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ):
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  padding: EdgeInsets.all(0),
                  children: <Widget>[
                    TextFormField(
                      // ignore: missing_return
                      validator: (input) {
                        if (input.isEmpty) {
                          return "Name is required";
                        }
                      },
                      controller: _buildingNameController,
                      style: TextStyle(color: Colors.black),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.text,
                      onSaved: (input) => _buildingName = input,
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(_focus);
                      },
                      cursorColor: Colors.blueGrey[800],
                      decoration: InputDecoration(
                        labelText: "Building Name",
                        labelStyle:
                        TextStyle(fontSize: 15, color: Colors.blueGrey[300]),
                        filled: true,
                        focusColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          borderSide: BorderSide(
                            color: Colors.transparent,
                          ),
                        ),
                        prefixIcon: Icon(Icons.home, color: Colors.blueGrey[300]),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          borderSide: BorderSide(
                            color: Colors.transparent,
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    TextFormField(
                      // ignore: missing_return
                      validator: (input) {
                        if (input.isEmpty) {
                          return "Floor is required";
                        }
                      },
                      controller: _buildingFloorController,
                      style: TextStyle(color: Colors.black),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      onSaved: (input) => _totalFloor = input,
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(_focus);
                      },
                      cursorColor: Colors.blueGrey[800],
                      decoration: InputDecoration(
                        labelText: "Total Floor",
                        labelStyle:
                        TextStyle(fontSize: 15, color: Colors.blueGrey[300]),
                        filled: true,
                        focusColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          borderSide: BorderSide(
                            color: Colors.transparent,
                          ),
                        ),
                        prefixIcon: Icon(Icons.layers, color: Colors.blueGrey[300]),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          borderSide: BorderSide(
                            color: Colors.transparent,
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    TextFormField(
                      // ignore: missing_return
                      validator: (input) {
                        if (input.isEmpty) {
                          return "Unit is required";
                        }
                      },
                      controller: _buildingUnitController,
                      style: TextStyle(color: Colors.black),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      onSaved: (input) => _totalUnit = input,
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(_focus);
                      },
                      cursorColor: Colors.blueGrey[800],
                      decoration: InputDecoration(
                        labelText: "Total Unit",
                        labelStyle:
                        TextStyle(fontSize: 15, color: Colors.blueGrey[300]),
                        filled: true,
                        focusColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          borderSide: BorderSide(
                            color: Colors.transparent,
                          ),
                        ),
                        prefixIcon: Icon(Icons.pie_chart, color: Colors.blueGrey[300]),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          borderSide: BorderSide(
                            color: Colors.transparent,
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    Container(
                      color: Colors.grey[200],
                      padding: EdgeInsets.fromLTRB(20, 5, 5, 5),
                      child: Row(
                        children: <Widget>[
                          Container(
                            child: Icon(Icons.merge_type, color: Colors.blueGrey[300]),
                            padding: EdgeInsets.only(top: 10, bottom: 10),
                          ),
                          type == null
                              ? Container()
                              : Expanded(
                            child: Container(
                              padding: EdgeInsets.only(left: 10),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                                  hint: Text(
                                    'Part of Apartment',
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.blueGrey[300]),
                                  ),
                                  value: _selectedLocation,
                                  style: Theme.of(context).textTheme.title,
                                  onChanged: (newValue) {
                                    if (!mounted) return;
                                    setState(() {
                                      FocusScope.of(context)
                                          .requestFocus(FocusNode());
                                      _selectedLocation = newValue;
                                      if (newValue == "Partial") {
                                        isFullApartment = false;
                                        generateFloor();
                                      }else{
                                        floorList.clear();
                                        unitList.clear();
                                        apartmentList.clear();
                                        isFullApartment = true;
                                      }
                                    });
                                  },
                                  isExpanded: true,
                                  items: type.map((f) {
                                    return DropdownMenuItem(
                                      child: new Text(
                                        f,
                                        style: TextStyle(
                                          color: Colors.blueGrey[800],
                                          fontSize: 15,
                                        ),
                                      ),
                                      value: f,
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 5),
                    floorList.length == 0
                        ? Container()
                        : Container(
                      color: Colors.grey[200],
                      padding: EdgeInsets.fromLTRB(20, 5, 5, 5),
                      child: Row(
                        children: <Widget>[
                          Container(
                            child: Icon(Icons.view_agenda,
                                color: Colors.blueGrey[300]),
                            padding: EdgeInsets.only(top: 10, bottom: 10),
                          ),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(left: 10),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                                  hint: Text(
                                    'Select Floor',
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.blueGrey[300]),
                                  ),
                                  value: selectedFloor,
                                  style: Theme.of(context).textTheme.bodyText1,
                                  onChanged: (newValue) {
                                    if (!mounted) return;
                                    setState(() {
                                      FocusScope.of(context)
                                          .requestFocus(FocusNode());
                                      selectedFloor = newValue;
                                      selectedUnit = null;
                                      generateUnit();
                                    });
                                  },
                                  isExpanded: true,
                                  items: floorList.map((f) {
                                    return DropdownMenuItem(
                                      child: new Text(
                                        f == 1
                                            ? f.toString() + "st Floor"
                                            : f == 2
                                            ? f.toString() + "nd Floor"
                                            : f == 3
                                            ? f.toString() + "ed Floor"
                                            : f.toString() + "th Floor",
                                        style: TextStyle(
                                          color: Colors.blueGrey[800],
                                          fontSize: 15,
                                        ),
                                      ),
                                      value: f.toString(),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 5),
                    unitList.length == 0
                        ? Container()
                        : Container(
                      color: Colors.grey[200],
                      padding: EdgeInsets.fromLTRB(20, 5, 5, 5),
                      child: Row(
                        children: <Widget>[
                          Container(
                            child: Icon(Icons.album,
                                color: Colors.blueGrey[300]),
                            padding: EdgeInsets.only(top: 10, bottom: 10),
                          ),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(left: 10),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                                  hint: Text(
                                    'Select Floor',
                                    style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.blueGrey[300]),
                                  ),
                                  value: selectedUnit,
                                  style: Theme.of(context).textTheme.title,
                                  onChanged: (newValue) {
                                    if (!mounted) return;
                                    setState(() {
                                      FocusScope.of(context)
                                          .requestFocus(FocusNode());
                                      selectedUnit = newValue;
                                      print(selectedUnit);
                                      if(!apartmentList.contains(selectedUnit)){
                                        apartmentList.add(selectedUnit);
                                      }
                                    });
                                  },
                                  isExpanded: true,
                                  items: unitList.map((f) {
                                    return DropdownMenuItem(
                                      child: new Text(
                                        selectedFloor + alphabet.elementAt(f - 1),
                                        style: TextStyle(
                                          color: Colors.blueGrey[800],
                                          fontSize: 15,
                                        ),
                                      ),
                                      value: selectedFloor + alphabet.elementAt(f - 1),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 5),
                    Center(
                      child: Wrap(
                          spacing: 10,
                          children: apartmentList.map((e){
                            return Chip(
                                backgroundColor: Colors.blueGrey,
                                label: Padding(
                                  padding: EdgeInsets.all(2),
                                  child: Text(e, style: TextStyle(color: Colors.white),),
                                ),
                                deleteIcon: Icon(
                                  Icons.close,
                                  color: Colors.white,
                                ),
                                onDeleted: () {
                                  setState(() {
                                    apartmentList.remove(e);
                                  });
                                });
                          }).toList()
                      ),
                    ),
                  ],
                ),
              ):
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  TextFormField(
                    // ignore: missing_return
                    validator: (input) {
                      if (input.isEmpty) {
                        return "Name is required";
                      }
                    },
                    controller: _buildingNameController,
                    style: TextStyle(color: Colors.black),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.text,
                    onSaved: (input) => _buildingName = input,
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).requestFocus(_focus);
                    },
                    cursorColor: Colors.blueGrey[800],
                    decoration: InputDecoration(
                      labelText: "Building Name",
                      labelStyle:
                      TextStyle(fontSize: 15, color: Colors.blueGrey[300]),
                      filled: true,
                      focusColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Colors.transparent,
                        ),
                      ),
                      prefixIcon: Icon(Icons.home, color: Colors.blueGrey[300]),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Colors.transparent,
                          width: 2.0,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  TextFormField(
                    // ignore: missing_return
                    validator: (input) {
                      if (input.isEmpty) {
                        return "Floor is required";
                      }
                    },
                    controller: _buildingFloorController,
                    style: TextStyle(color: Colors.black),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.number,
                    onSaved: (input) => _totalFloor = input,
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).requestFocus(_focus);
                    },
                    cursorColor: Colors.blueGrey[800],
                    decoration: InputDecoration(
                      labelText: "Total Floor",
                      labelStyle:
                      TextStyle(fontSize: 15, color: Colors.blueGrey[300]),
                      filled: true,
                      focusColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Colors.transparent,
                        ),
                      ),
                      prefixIcon: Icon(Icons.layers, color: Colors.blueGrey[300]),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Colors.transparent,
                          width: 2.0,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  TextFormField(
                    // ignore: missing_return
                    validator: (input) {
                      if (input.isEmpty) {
                        return "Unit is required";
                      }
                    },
                    controller: _buildingUnitController,
                    style: TextStyle(color: Colors.black),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.number,
                    onSaved: (input) => _totalUnit = input,
                    onFieldSubmitted: (v) {
                      FocusScope.of(context).requestFocus(_focus);
                    },
                    cursorColor: Colors.blueGrey[800],
                    decoration: InputDecoration(
                      labelText: "Total Unit",
                      labelStyle:
                      TextStyle(fontSize: 15, color: Colors.blueGrey[300]),
                      filled: true,
                      focusColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Colors.transparent,
                        ),
                      ),
                      prefixIcon: Icon(Icons.pie_chart, color: Colors.blueGrey[300]),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: Colors.transparent,
                          width: 2.0,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                ],
              ) :
            widget.isUpdate ? Container():
            Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(child: Text("Building Name: ", style: TextStyle(color: Colors.blueGrey[900], fontWeight: FontWeight.bold),), flex: 2,),
                        Expanded(child: Text(_buildingName), flex: 3,)
                      ],
                    ),
                  ),
                  Divider(
                    height: 5,
                    color: Colors.grey,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(child: Text("Total Floor: ", style: TextStyle(color: Colors.blueGrey[900], fontWeight: FontWeight.bold),), flex: 2,),
                        Expanded(child: Text(_totalFloor), flex: 3,)
                      ],
                    ),
                  ),
                  Divider(
                    height: 5,
                    color: Colors.grey,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(child: Text("Total Unit: ", style: TextStyle(color: Colors.blueGrey[900], fontWeight: FontWeight.bold)), flex: 2,),
                        Expanded(child: Text(_totalUnit), flex: 3,)
                      ],
                    ),
                  ),
                  Divider(
                    height: 5,
                    color: Colors.grey,
                  ),
                  Padding(
                      padding: EdgeInsets.all(8),
                    child: Row(
                      children: <Widget>[
                        Expanded(child: Text("Flat's: ", style: TextStyle(color: Colors.blueGrey[900], fontWeight: FontWeight.bold)), flex: 2,),
                        Expanded(
                          flex: 3,
                          child: apartmentList.length > 0 ? Wrap(
                              spacing: 3,
                              children: apartmentList.map((e){
                                return Chip(
                                    backgroundColor: Colors.grey[200],
                                    label: Text(e, style: TextStyle(fontSize: 12, color: Colors.black87),),
                                );
                              }).toList()
                          ):
                          Chip(
                            backgroundColor: Colors.grey[200],
                            label: Padding(
                              padding: EdgeInsets.all(2),
                              child: Text("Full Building", style: TextStyle(fontSize: 12, color: Colors.black87),),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Divider(
              height: 15,
              color: Colors.grey,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                 FlatButton(
                  color: Colors.grey[200],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                  ),
                    onPressed: () {
                      if(widget.isUpdate){
                         Navigator.pop(context);
                      }else{
                        if(isPreview){
                      setState(() {
                        isPreview = false;
                        _buildingNameController.text = _buildingName;
                        _buildingFloorController.text = _totalFloor;
                        _buildingUnitController.text = _totalUnit;
                      });
                    }else{
                      Navigator.pop(context);
                    }
                      }
                    },
                    child: Text("Close")),
                SizedBox(
                  width: 5,
                ),

                widget.isCreate ?
                isPreview ?
                FlatButton(
                  disabledColor: Colors.blue[200],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)
                    ),
                    color: Colors.blue,
                    onPressed: () {
                     setState(() {
                        createBuilding();
                     });
                    },
                    child: Text("Submit",
                    style: TextStyle(
                      color: Colors.white
                    ),)):
                FlatButton(
                    disabledColor: Colors.blue[200],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)
                    ),
                    color: Colors.blue,
                    onPressed: apartmentList.length > 0 || isFullApartment? () {
                        var formState = _kayState.currentState;
                        if (formState.validate()) {
                          formState.save();
                          setState(() {
                            isPreview = true;
                          });
                        }
                    } : null,
                    child: Text("Preview",
                      style: TextStyle(
                          color: Colors.white
                      ),)):
                    widget.isAdd?
                    isPreview ?
                    FlatButton(
                        disabledColor: Colors.blue[200],
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                        ),
                        color: Colors.blue,
                        onPressed: () {
                          setState(() {
                            createBuilding();
                          });
                        },
                        child: Text("Submit",
                          style: TextStyle(
                              color: Colors.white
                          ),)):
                        floor == null?
                        FlatButton(
                            disabledColor: Colors.blue[200],
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)
                            ),
                            color: Colors.green,
                            onPressed:() {
                              var formState = _kayState.currentState;
    if (formState.validate()) {
      formState.save();
                              getFloorById();
    }
                            },
                            child: Text("Submit",
                              style: TextStyle(
                                  color: Colors.white
                              ),)):
                    FlatButton(
                        disabledColor: Colors.blue[200],
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                        ),
                        color: Colors.blue,
                        onPressed: apartmentList.length > 0 || isFullApartment? () {
                          var formState = _kayState.currentState;
                          if (formState.validate()) {
                            formState.save();
                            setState(() {
                              isPreview = true;
                            });
                          }
                        } : null,
                        child: Text("Preview",
                          style: TextStyle(
                              color: Colors.white
                          ),)):
                    isPreview ?
                    FlatButton(
                        disabledColor: Colors.blue[200],
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                        ),
                        color: Colors.blue,
                        onPressed: () {
                          setState(() {
                            createBuilding();
                          });
                        },
                        child: Text("Submit",
                          style: TextStyle(
                              color: Colors.white
                          ),)):
                    FlatButton(
                        disabledColor: Colors.blue[200],
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                        ),
                        color: Colors.blue,
                        onPressed: apartmentList.length > 0 || isFullApartment? () {
                          var formState = _kayState.currentState;
                          if (formState.validate()) {
                            formState.save();
                            setState(() {
                              isPreview = true;
                            });
                          }
                        } : null,
                        child: Text("Preview",
                          style: TextStyle(
                              color: Colors.white
                          ),))
              ],
            ),
          ],
        ),
      ),
    );
  }
}
