import 'dart:async';
import 'dart:io';
import 'package:House/model/ImageCropeShape.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class CameraApp extends StatefulWidget {
  final Function sendImagePath;
  final shape cropShape;
  CameraApp({Key key, this.cropShape, this.sendImagePath}) : super(key: key);

  @override
  _CameraAppState createState() => _CameraAppState();
}

class _CameraAppState extends State<CameraApp> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  CameraController controller;
  Future<void> _initializeControllerFuture;
  List<CameraDescription> cameras;
  bool cameraGot = false;
  Size size;

  bool enableAudio = true;

  String imageFile;

  var savedImagePath;
  bool isFirstImageTake = false;

  String notifyText,
      img1Text = "Take NID Front Side Image",
      img2Text = "Take NID Back Page Image",
  proText = "Take Your Profile Pic";

  bool startProgress = false;

  Future<bool> getCamera() async {
    cameras = await availableCameras();
    setState(() {
      this.cameras = cameras;
      this.cameraGot = true;
    });
    return true;
  }

  @override
  void initState() {
    super.initState();
    if(widget.cropShape == shape.Circle){
      notifyText = proText;
    }else{
      notifyText = img1Text;
    }
    getCamera().then((value) {
      if (this.cameraGot) {
        if (this.cameras.length > 1) {
          if(widget.cropShape == shape.Circle){
            controller = new CameraController(this.cameras[1], ResolutionPreset.veryHigh);
            _initializeControllerFuture = controller.initialize().then((value) {
              if(!mounted) return;
              setState(() {});
            });
          }else{
            controller = new CameraController(this.cameras[0], ResolutionPreset.veryHigh);
            _initializeControllerFuture = controller.initialize().then((value) {
              if(!mounted) return;
              setState(() {});
            });
          }
        } else {
          controller = new CameraController(this.cameras[0], ResolutionPreset.veryHigh);
          _initializeControllerFuture = controller.initialize().then((value) {
            if(!mounted) return;
            setState(() {});
          });
        }
      }
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void onTakePictureButtonPressed(context) async{
    await _initializeControllerFuture;
    takePicture().then((String filePath) async {
      if (!mounted) return;
          GallerySaver.saveImage(filePath);
          File croppedFile = await ImageCropper.cropImage(
              sourcePath: filePath,
              aspectRatio: CropAspectRatio(ratioX: 3, ratioY: 2),
              compressQuality: 100,
              compressFormat: ImageCompressFormat.png,
              cropStyle: widget.cropShape == shape.Rectangle ? CropStyle.rectangle : CropStyle.circle,
              androidUiSettings: AndroidUiSettings(
                  toolbarTitle: 'Griho Cropper',
                  toolbarColor: Colors.deepOrange,
                  toolbarWidgetColor: Colors.white,
                  initAspectRatio: CropAspectRatioPreset.original,
                  backgroundColor: Colors.white
              ),
              iosUiSettings: IOSUiSettings(
                minimumAspectRatio: 1.0,
              )
          );
         await croppedFile.copy(filePath).then((value){
           setState(() {
             imageFile = filePath;
             print("Camera Aspect Ratio : ${controller.value.aspectRatio}");
             startProgress = true;
             if(widget.cropShape == shape.Circle){
               widget.sendImagePath(filePath, true);
               Timer(Duration(milliseconds: 200), (){
                 setState(() {
                   startProgress = false;
                   Navigator.pop(context);
                 });
               });
             }else{
               widget.sendImagePath(filePath, false);
               if(!isFirstImageTake){
                 Timer(Duration(milliseconds: 200), (){
                   setState(() {
                     startProgress = false;
                     isFirstImageTake = true;
                     notifyText = img2Text;
                    controller.initialize().then((value) {
                      if(!mounted) return;
                      setState(() {});
                    });
                   });
                 });
               }else{
                 Timer(Duration(milliseconds: 200), (){
                   setState(() {
                     startProgress = false;
                     Navigator.pop(context);
                   });
                 });
               }
             }
           });
         });
    });
  }

  Future<String> takePicture() async {
    if (!controller.value.isInitialized) {
      return null;
    }
    try {
      final path = join(
        (await getTemporaryDirectory()).path, //Temporary path
        '${timestamp()}.png',
      );
      savedImagePath = path;
      await controller.takePicture(path); //take photo
      setState(() {});
    } catch (e) {
      print(e);
    }
    return savedImagePath;
  }

  void logError(String code, String message) =>
      print('Error: $code\nError Message: $message');

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    if (controller == null || !controller.value.isInitialized) {
      return Container();
    }
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            child: FutureBuilder<void>(
              future: _initializeControllerFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  // If the Future is complete, display the preview.
                  return CameraPreview(controller);
                } else {
                  // Otherwise, display a loading indicator.
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
          ),
          widget.cropShape == shape.Circle ?
          Positioned(
            top: -150,
            bottom: -150,
            left: -150,
            right: -150,
            child:
            Container(
              decoration: BoxDecoration(
                  shape: widget.cropShape == shape.Circle ? BoxShape.circle : BoxShape.rectangle,
                  border: Border.all(width: size.height * .25, color:  Colors.black.withOpacity(.6),)),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: size.width,
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: size.height * .1,
                        bottom: size.height * .1,
                        left: size.height * .02,
                        right: size.height * .02),
                    width: size.width,
                    decoration: BoxDecoration(
                      shape: widget.cropShape == shape.Circle ? BoxShape.circle : BoxShape.rectangle,
                        border: Border.all(color: Colors.lightGreenAccent)),
                  )
                ],
              ),
            ),
          ):
          Positioned(
            top: size.height * .2,
            bottom: size.height * .2,
            left: 0,
            right: 0,
            child:
            Stack(
              children: <Widget>[
                Container(
                  width: size.width,
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(width: size.height * .14, color: Colors.black.withOpacity(.6)),
                      bottom: BorderSide(width: size.height * .14, color: Colors.black.withOpacity(.6)),
                      left: BorderSide(width: size.height * .02, color: Colors.black.withOpacity(.6)),
                      right: BorderSide(width: size.height * .02, color: Colors.black.withOpacity(.6)),
                    )
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      top: size.height * .14,
                      bottom: size.height * .14,
                      left: size.height * .02,
                      right: size.height * .02),
                  width: size.width,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.lightGreenAccent)),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              color: Colors.blueGrey[900],
              height: size.height * .2,
              child: Center(
                  child: Text(
                    notifyText,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold),
                  )),
            ),
          ),
//          imageFile == null ? Container() :
//          Container(
//            child: Image.file(File(imageFile)),
//          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              color: Colors.blueGrey[900],
              height: size.height * .2,
              child: Center(
                child: InkWell(
                    onTap: () {
                      if (controller != null &&
                          controller.value.isInitialized) {
                        onTakePictureButtonPressed(context);
                      }
                    },
                    child: Icon(
                      Icons.camera,
                      size: 60,
                      color: Colors.cyanAccent,
                    )),
              ),
            ),
          ),
          startProgress? Positioned(
            top: 0,
            bottom: 0,
            child: Container(
              height: size.height,
              width: size.width,
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(
                  valueColor:
                  AlwaysStoppedAnimation<
                      Color>(
                      Colors.black),
                ),
              ),
            ),
          ): Container()
        ],
      ),
    );
  }
}
