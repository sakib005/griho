import 'package:flutter/material.dart';

class MyCircleAvatar extends StatelessWidget {

  final AssetImage assetImage;
  final NetworkImage networkImage;
  final FileImage fileImage;
  final double radius;

  MyCircleAvatar({this.assetImage, this.networkImage, this.fileImage, this.radius});

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: radius + 5,
      backgroundColor: Colors.white,
      child: CircleAvatar(
        radius: radius,
        backgroundImage: assetImage == null ? networkImage == null ? fileImage : networkImage : assetImage,
      ),
    );
  }
}
