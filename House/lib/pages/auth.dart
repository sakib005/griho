import 'dart:math';

import 'package:House/pages/sign_in.dart';
import 'package:House/pages/sign_up.dart';
import 'package:House/plugin/scale_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AuthActivity extends StatefulWidget {
  @override
  _AuthActivityState createState() => _AuthActivityState();
}

class _AuthActivityState extends State<AuthActivity> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()=> SystemNavigator.pop(),
      child: SafeArea(child: Scaffold(
        body: Stack(
          children: <Widget>[
            Positioned(
                right: -50,
                top: -50,
                child: Image.asset("assets/leaf.png", width: 250)),
            Container(
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset("assets/logo.png", scale: 4.5,),
                    SizedBox(
                      height: 20,
                    ),
                    Text("Let't get started", style: TextStyle(),),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.push(context, ScaleRoute(page: SignUp()));
                      },
                      child: Container(
                        width: 120,
                        decoration: BoxDecoration(
                          color: Colors.green[800],
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.green.withOpacity(0.5),
                              spreadRadius: 1,
                              blurRadius: 4,
                              offset: Offset(0, 3),
                            )
                          ]
                        ),
                        padding: EdgeInsets.all(10),
                        child: Center(child: Text("SIGN UP", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.push(context, ScaleRoute(page: SignIn()));
                      },
                      child: Container(
                        width: 120,
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 4,
                                offset: Offset(0, 3),
                              )
                            ]
                        ),
                        padding: EdgeInsets.all(10),
                        child: Center(child: Text("SIGN IN", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),)),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        )
      )),
    );
  }
}
