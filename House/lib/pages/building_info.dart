import 'package:House/model/floor.dart';
import 'package:House/model/floor_details.dart';
import 'package:House/service/rest_service.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BuildingInfo extends StatefulWidget {
  Info floor;

  BuildingInfo({Key key, this.floor}) : super(key: key);

  @override
  _BuildingInfoState createState() => _BuildingInfoState();
}

class _BuildingInfoState extends State<BuildingInfo>
    with SingleTickerProviderStateMixin {
  RestService get restService => GetIt.I<RestService>();

  ScrollController _controllers = ScrollController();

  AnimationController _controller;
  Animation<Offset> _offsetAnimation;
  Animation _fadeAnimation;

  var list = new List<int>();

  Size size;

  bool isClick = false;

  double roofH = 30,
      roofW = 120,
      sideH = 30,
      sideW = 30,
      blockH = 30,
      blockW = 121,
      topP = 33;

  int selectedApart = -1;

  var floors = [];
  String houseId, userId;
  FloorDetails floorDetails = FloorDetails();

  generateBuilding() {
    setState(() {
      list = new List<int>.generate(
          widget.floor.totalFloorOfBuilding, (i) => i + 1);
      floors = widget.floor.useAbleFloors;
      houseId = widget.floor.houseId.toString();
    });
  }

  @override
  void didUpdateWidget(BuildingInfo oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    if(oldWidget.floor != widget.floor){
      generateBuilding();
    }
  }

  @override
  void initState() {
    super.initState();
    getUserInfo();
    generateBuilding();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 400),
      vsync: this,
    );
    _offsetAnimation = Tween<Offset>(
      begin: const Offset(0.25, 0.0),
      end: const Offset(0.0, 0.0),
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeIn,
    ));
    _fadeAnimation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(CurvedAnimation(parent: _controller, curve: Curves.easeIn));
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  getUserInfo() async {
    final prefs = await SharedPreferences.getInstance();
    userId = prefs.getString("userId");
  }

  getFloorDetails(floor) async {
    var data = await restService.getFloorDetails(
        id: userId, houseid: houseId, floor: floor);
    if (!data.error) {
      if (!mounted) return;
      setState(() {
        floorDetails = data.data;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return
      list == null ? Container() :
      Container(
        child: Stack(
          children: <Widget>[
            SlideTransition(
                position: _offsetAnimation,
                child: Container(
                    margin: EdgeInsets.only(top: size.height / 5),
                    child: Center(
                      child: Stack(
                          children: list.map((e) {
                            if (list.indexOf(e) == 0) {
                              return Positioned(
                                top: list.indexOf(e) * 60.0,
                                left: 45,
                                child: Container(
                                  child: Transform(
                                    transform: Matrix4.skewX(-.7),
                                    child: Container(
                                      height: roofH,
                                      width: roofW,
                                      color: Colors.brown[400],
                                    ),
                                  ),
                                ),
                              );
                            } else {
                              return floors.contains(
                                  (list.length - list.indexOf(e))) ==
                                  true
                                  ? Positioned(
                                  top: list.indexOf(e) * topP,
                                  left: 10,
                                  child: Stack(
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(left: 130),
                                        child: Transform(
                                          transform: Matrix4.skewY(-.85),
                                          child: Container(
                                            height: sideH,
                                            width: sideW,
                                            color: Colors.brown[700],
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              if (isClick) {
                                                isClick = false;
                                                selectedApart = -1;
                                                _controller.reverse();
                                                floorDetails = new FloorDetails();
                                              } else {
                                                isClick = true;
                                                selectedApart =
                                                    list.length - list.indexOf(e);
                                                getFloorDetails(list.length -
                                                    list.indexOf(e));
                                                _controller.forward();
                                              }
                                            });
                                          },
                                          child: (list.length -
                                              list.indexOf(e)) !=
                                              selectedApart
                                              ?
                                          Container(
                                            margin:
                                            EdgeInsets.only(left: 10),
                                            height: blockH,
                                            width: blockW,
                                            color: Colors.brown[700],
                                            child: Center(
                                                child: Text(
                                                  (list.length - list.indexOf(e)) == 1
                                                      ? (list.length - list.indexOf(e))
                                                      .toString() +
                                                      "st floor"
                                                      : (list.length -
                                                      list.indexOf(
                                                          e)) ==
                                                      2
                                                      ? (list.length -
                                                      list.indexOf(
                                                          e))
                                                      .toString() +
                                                      "nd floor"
                                                      : (list.length -
                                                      list.indexOf(
                                                          e)) ==
                                                      3
                                                      ? (list.length - list.indexOf(e))
                                                      .toString() +
                                                      "ed floor"
                                                      : (list.length -
                                                      list.indexOf(e))
                                                      .toString() +
                                                      "th floor",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                      FontWeight.bold),
                                                )),
                                          )
                                              : Container(
                                            margin:
                                            EdgeInsets.only(left: 10),
                                            height: blockH,
                                            width: blockW,
                                            color: Colors.brown[200],
                                            child: Center(
                                                child: Text(
                                                  (list.length - list.indexOf(e)) == 1
                                                      ? (list.length - list.indexOf(e))
                                                      .toString() +
                                                      "st floor"
                                                      : (list.length -
                                                      list.indexOf(
                                                          e)) ==
                                                      2
                                                      ? (list.length -
                                                      list.indexOf(
                                                          e))
                                                      .toString() +
                                                      "nd floor"
                                                      : (list.length -
                                                      list.indexOf(
                                                          e)) ==
                                                      3
                                                      ? (list.length - list.indexOf(e))
                                                      .toString() +
                                                      "ed floor"
                                                      : (list.length -
                                                      list.indexOf(e))
                                                      .toString() +
                                                      "th floor",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                      FontWeight.bold),
                                                )),
                                          )),
                                    ],
                                  ))
                                  : Positioned(
                                  top: list.indexOf(e) * topP,
                                  left: 10,
                                  child: Stack(
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(left: 130),
                                        child: Transform(
                                          transform: Matrix4.skewY(-.85),
                                          child: Container(
                                            height: sideH,
                                            width: sideW,
                                            color: Colors.brown[600],
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              if (isClick) {
                                                isClick = false;
                                                selectedApart = -1;
                                                _controller.reverse();
                                                floorDetails = new FloorDetails();
                                              } else {
                                                isClick = true;
                                                selectedApart =
                                                    list.length - list.indexOf(e);
                                                getFloorDetails(list.length -
                                                    list.indexOf(e));
                                                _controller.forward();
                                              }
                                            });
                                          },
                                          child: (list.length -
                                              list.indexOf(e)) !=
                                              selectedApart
                                              ?
                                          Container(
                                            margin:
                                            EdgeInsets.only(left: 10),
                                            height: blockH,
                                            width: blockW,
                                            color: Colors.brown[400],
                                            child: Center(
                                                child: Text(
                                                  (list.length - list.indexOf(e)) == 1
                                                      ? (list.length - list.indexOf(e))
                                                      .toString() +
                                                      "st floor"
                                                      : (list.length -
                                                      list.indexOf(
                                                          e)) ==
                                                      2
                                                      ? (list.length -
                                                      list.indexOf(
                                                          e))
                                                      .toString() +
                                                      "nd floor"
                                                      : (list.length -
                                                      list.indexOf(
                                                          e)) ==
                                                      3
                                                      ? (list.length - list.indexOf(e))
                                                      .toString() +
                                                      "ed floor"
                                                      : (list.length -
                                                      list.indexOf(e))
                                                      .toString() +
                                                      "th floor",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                      FontWeight.bold),
                                                )),
                                          )
                                              : Container(
                                            margin:
                                            EdgeInsets.only(left: 10),
                                            height: blockH,
                                            width: blockW,
                                            color: Colors.brown[200],
                                            child: Center(
                                                child: Text(
                                                  (list.length - list.indexOf(e)) == 1
                                                      ? (list.length - list.indexOf(e))
                                                      .toString() +
                                                      "st floor"
                                                      : (list.length -
                                                      list.indexOf(
                                                          e)) ==
                                                      2
                                                      ? (list.length -
                                                      list.indexOf(
                                                          e))
                                                      .toString() +
                                                      "nd floor"
                                                      : (list.length -
                                                      list.indexOf(
                                                          e)) ==
                                                      3
                                                      ? (list.length - list.indexOf(e))
                                                      .toString() +
                                                      "ed floor"
                                                      : (list.length -
                                                      list.indexOf(e))
                                                      .toString() +
                                                      "th floor",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                      FontWeight.bold),
                                                )),
                                          )),
                                    ],
                                  ));
                            }
                          }).toList()),
                    ))),
            Positioned(
            bottom: 130,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  width: size.width,
                    child: Center(
                        child: Text("A.N.: "+ widget.floor.houseName,
                        style: TextStyle(
                          color: Colors.blueGrey[900],
                          fontWeight: FontWeight.bold,
                          fontSize: 20
                        ),)
                    )
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                    width: size.width,
                    child: Center(
                        child: Text("Code: "+ widget.floor.houseId.toString(),
                          style: TextStyle(
                              color: Colors.blueGrey[900],
                              fontWeight: FontWeight.bold,
                              fontSize: 15
                          ),)
                    )
                ),
              ],
            ),
          ),
            Positioned(
            left: MediaQuery.of(context).size.width / 2.2,
            child: FadeTransition(
                opacity: _fadeAnimation,
                child: Container(
                    margin: EdgeInsets.only(top: size.height / 5),
                    padding: EdgeInsets.all(5),
                    height: size.height * .45,
                    width: size.width * .52,
                    child: floorDetails.result == null
                        ? Container()
                        : ListView(
                            padding: EdgeInsets.only(top: 0),
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            children: floorDetails.result.map((e) {
                              return Container(
                                margin: EdgeInsets.only(bottom: 5),
                                padding: EdgeInsets.only(
                                    left: 5, top: 5, right: 5, bottom: 10),
                                decoration: BoxDecoration(
                                    color: Colors.brown[500],
                                    borderRadius: BorderRadius.circular(8)),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Expanded(
                                                child: Container(
                                                    padding: EdgeInsets.all(10),
                                                    child: Text(
                                                      "Ap No: " + e.apartmentNo,
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    )),
                                              ),
                                              Container(
                                                child: Text(
                                                  e.familyMembers.toString(),
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                                padding: EdgeInsets.fromLTRB(
                                                    15, 5, 15, 5),
                                                color: Colors.blueGrey[600]
                                                    .withOpacity(.2),
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Icon(
                                                Icons.group_add,
                                                color: Colors.orange,
                                              ),
                                              SizedBox(
                                                width: 3,
                                              )
                                            ],
                                          ),
                                          Divider(
                                            height: 1,
                                            color: Colors.grey[300],
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(5.0),
                                                child: CircleAvatar(
                                                  backgroundImage:
                                                      NetworkImage(e.userImage),
                                                  radius: 15,
                                                ),
                                              ),
                                              Expanded(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .stretch,
                                                  children: <Widget>[
                                                    Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 10,
                                                                top: 10),
                                                        child: Text(
                                                          e.userName,
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white),
                                                        )),
                                                    Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 10,
                                                                top: 3),
                                                        child: Row(
                                                          children: <Widget>[
                                                            Expanded(
                                                                child: Text(
                                                              e.mobileNo,
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white),
                                                            )),
                                                            CircleAvatar(
                                                              child: Icon(
                                                                Icons.phone,
                                                                size: 12,
                                                              ),
                                                              radius: 15,
                                                              backgroundColor:
                                                                  Colors.brown[
                                                                      800],
                                                            )
                                                          ],
                                                        )),
                                                    Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 10,
                                                                top: 3),
                                                        child: Text(
                                                          e.occupation,
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white),
                                                        )),
                                                  ],
                                                ),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }).toList(),
                          ))),
          ),
        ],
      ),
    );
  }
}
