import 'package:House/model/building.dart';
import 'package:House/model/floor.dart';
import 'package:House/model/privacy.dart';
import 'package:House/model/user_profile.dart';
import 'package:House/plugin/mycircleavater.dart';
import 'package:House/service/rest_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OwnerPost extends StatefulWidget {

  final Function onNoticeUpdate;
  final Floor buildings;

  OwnerPost({Key key, this.buildings, this.onNoticeUpdate}): super(key: key);

  @override
  _PostState createState() => _PostState();
}

class _PostState extends State<OwnerPost> {
  RestService get restService => GetIt.I<RestService>();
  String _message, _privacy;
  final _focus = FocusNode();
  final GlobalKey<FormState> _kayState = GlobalKey<FormState>();
  TextEditingController _controller = new TextEditingController();

  String choice;
  Privacy privacy = Privacy();
  String privacyVal = "";
  int privacyId = 0;

  String _selectedLocation;

  UserProfile profile;

  String houseId, userId;

  getUserInfo() async{
    final prefs = await SharedPreferences.getInstance();
    userId =  prefs.getString('userId');
    var data = await restService.getProfile(id: userId);
    if(!data.error){
      setState(() {
        profile = data.data;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getUserInfo();
  }

  void postIssue() async {
    var formState = _kayState.currentState;
    if (formState.validate()) {
      formState.save();
      var tt = _controller.text;
      if(tt != ""){
        final result =
        await restService.ownerPostNotice(userId: userId, houseId :houseId, message: tt);
        if (!result.error) {
          widget.onNoticeUpdate();
          Navigator.pop(context);
        }
      }else{
        Fluttertoast.showToast(
            msg: "Sorry! you cannot post empty issue",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap: (){
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text("Create Post"),
            backgroundColor: Colors.white,
            actions: <Widget>[
              Center(
                child: GestureDetector(
                  onTap: (){
                    postIssue();
                  },
                  child: Container(
                      padding: EdgeInsets.all(10),
                      child: Text("POST", style: TextStyle(color: Colors.grey),)
                  ),
                ),
              )
            ],
          ),
          body: profile == null?
          Container() :
          Stack(
            children: <Widget>[
              GestureDetector(
                onTap: (){

                },
                child: Container(
                    padding: EdgeInsets.all(15),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              child: MyCircleAvatar(
                                radius: 25,
                                networkImage: NetworkImage(profile.result.userImage),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                                child: Container(
                                  child: Column(
                                    children: <Widget>[
                                      Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(profile == null ? "Guest" : profile.result.name, style: TextStyle(color: Colors.blueGrey, fontSize: 20, fontWeight: FontWeight.bold),)),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Container(
                                          padding: EdgeInsets.fromLTRB(10,0,0,0),
                                          decoration: BoxDecoration(
                                              border: Border.all(color: Colors.grey),
                                              borderRadius: BorderRadius.circular(5)
                                          ),
                                          child: Container(
                                            padding: EdgeInsets.only(left: 5),
                                            child: DropdownButtonHideUnderline(
                                              child: DropdownButton(
                                                hint: Text(
                                                  'Select Apartment',
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color: Colors.blueGrey[300]),
                                                ),
                                                value: _selectedLocation,
                                                style: Theme.of(context).textTheme.title,
                                                onChanged: (newValue) {
                                                  if (!mounted) return;
                                                  setState(() {
                                                    FocusScope.of(context)
                                                        .requestFocus(FocusNode());
                                                    _selectedLocation = newValue.toString();
                                                    houseId = widget.buildings.result.firstWhere((element) => element.houseName == newValue).houseId.toString();
                                                    print(_selectedLocation);
                                                  });
                                                },
                                                isExpanded: true,
                                                items: widget.buildings.result.map((f) {
                                                  return DropdownMenuItem(
                                                    child: new Text(
                                                      f.houseName,
                                                      style: TextStyle(
                                                        color: Colors.blueGrey[800],
                                                        fontSize: 15,
                                                      ),
                                                    ),
                                                    value: f.houseName,
                                                  );
                                                }).toList(),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Form(
                          key: _kayState,
                          child: TextField(
                            controller: _controller,
                            minLines: 7,
                            maxLines: 10,
                            decoration: InputDecoration(
                              hintText:
                              "আপনার ম্যাসেজটি লিখুন...",
                              border: InputBorder.none,
                            ),
                          ),
                        )
                      ],
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
