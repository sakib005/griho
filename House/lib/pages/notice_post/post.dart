import 'package:House/model/privacy.dart';
import 'package:House/pages/privacy_policy.dart';
import 'package:House/plugin/mycircleavater.dart';
import 'package:House/plugin/scale_route.dart';
import 'package:House/service/rest_service.dart';
import 'package:House/style_guid/app_background.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Post extends StatefulWidget {

  final Function onNoticeUpdate;

  Post({Key key, this.onNoticeUpdate}): super(key: key);

  @override
  _PostState createState() => _PostState();
}

class _PostState extends State<Post> {
  RestService get restService => GetIt.I<RestService>();
  String _message, _privacy;
  final _focus = FocusNode();
  final GlobalKey<FormState> _kayState = GlobalKey<FormState>();
  TextEditingController _controller = new TextEditingController();

  String _radioValue;
  String choice;
  Privacy privacy = Privacy();
  String privacyVal = "";
  int privacyId = 0;

  String houseId, userId;
  getUserInfo() async{
    final prefs = await SharedPreferences.getInstance();
    userId =  prefs.getString('userId');
    houseId =  prefs.getString('houseId');
    getPrivacy();
  }

  @override
  void initState() {
    super.initState();
    getUserInfo();
  }

  getPrivacy() async {
    var data = await restService.getPrivacy();
    if (!data.error) {
      setState(() {
        privacy = data.data;
        privacyVal = privacy.result[0].value;
        privacyId = privacy.result[0].key;
      });
    }
  }

  void postIssue() async {
    var formState = _kayState.currentState;
    if (formState.validate()) {
      formState.save();
      var tt = _controller.text;
      if(tt != ""){
        final result =
        await restService.postMessage(id: userId, houseid :houseId, message: tt, privacyType:privacyId);
        if (result.error) {
        } else {
          widget.onNoticeUpdate();
          Navigator.pop(context);
        }
      }else{
        Fluttertoast.showToast(
            msg: "Sorry! you cannot post empty issue",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    }
  }

  _onSelected(id){
     var pri = privacy.result.firstWhere((element) => element.key == id);
     setState(() {
       privacyId = pri.key;
       privacyVal = pri.value;
     });
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap: (){
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text("Create Post"),
            backgroundColor: Colors.white,
            actions: <Widget>[
              Center(
                child: GestureDetector(
                  onTap: (){
                    postIssue();
                  },
                  child: Container(
                    padding: EdgeInsets.all(10),
                      child: Text("POST", style: TextStyle(color: Colors.grey),)
                  ),
                ),
              )
            ],
          ),
          body: Stack(
            children: <Widget>[
              AppBackground(),
              GestureDetector(
                onTap: (){

                },
                child: Container(
                    padding: EdgeInsets.all(15),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              child: MyCircleAvatar(
                                radius: 25,
                                assetImage: AssetImage("assets/user.png"),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                                child: Container(
                              child: Column(
                                children: <Widget>[
                                  Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("Name", style: TextStyle(color: Colors.blueGrey, fontSize: 20, fontWeight: FontWeight.bold),)),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      width: 130,
                                      padding: EdgeInsets.fromLTRB(10,2,2,2),
                                      decoration: BoxDecoration(
                                        border: Border.all(color: Colors.grey),
                                        borderRadius: BorderRadius.circular(5)
                                      ),
                                      child: GestureDetector(
                                        onTap: (){
                                          Navigator.push(context, SlideRightRoute(page: PrivacyPolicy(privacy: privacy, selected: privacyId, onSelected: _onSelected,)));
                                        },
                                        child: Row(
                                          children: <Widget>[
                                            privacyId == 1 ? Image.asset("assets/gloab.png", width: 12,color: Colors.grey[700],) : Icon(Icons.security, size: 15,color: Colors.grey[700]),
                                            SizedBox(width: 5,),
                                            Expanded(child: Center(child: Text(privacyVal,  style: TextStyle(color: Colors.grey[700], fontSize: 12),))),
                                            Icon(Icons.arrow_drop_down, color: Colors.grey[700],)
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ))
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Form(
                          key: _kayState,
                          child: TextField(
                            controller: _controller,
                            minLines: 7,
                            maxLines: 10,
                            decoration: InputDecoration(
                                hintText:
                                "আপনার সমস্যাটি লিখুন...",
                                border: InputBorder.none,
                            ),
                          ),
                        )
                      ],
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
