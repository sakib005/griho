import 'dart:io';

import 'package:House/main.dart';
import 'package:House/model/ImageCropeShape.dart';
import 'package:House/plugin/camera.dart';
import 'package:House/plugin/mycircleavater.dart';
import 'package:House/service/rest_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileActivity extends StatefulWidget {
  final Function isBottomShow;

  ProfileActivity({Key key, this.isBottomShow}) : super(key: key);

  @override
  _ProfileActivityState createState() => _ProfileActivityState();
}

class _ProfileActivityState extends State<ProfileActivity> {
  RestService get restService => GetIt.I<RestService>();
  String _designation, _familyMember, _nid;

  TextEditingController _designationController = TextEditingController(),
      _familyMembersController = TextEditingController(),
      _familyMembersOccController = TextEditingController();

  Size size;
  bool isUpdating = true;
  String _extractText = 'Unknown';

  var images = [];
  bool isProfileSet = false;

  var userId, userType;

  String profileImage, username, phone;

  @override
  void initState() {
    super.initState();
    getUserInfo();
  }

  getUserInfo() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      userId = prefs.getString('userId');
      userType = prefs.getString("userType");
      username = prefs.getString('username');
      phone = prefs.getString("phone");
    });
  }

  setImagePath(path, isProfile) {
    if(!mounted) return;
    setState(() {
      if (isProfile) {
        isProfileSet = true;
        profileImage = path;
        images.insert(0, path);
      } else {
        images.add(path);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      bottomNavigationBar: Container(
        height: 0,
      ),
      body: Stack(
        children: <Widget>[
          Container(
            color: Colors.white,
          ),
          Container(
            height: size.height * .25,
            decoration: BoxDecoration(
              color: Colors.blueGrey[900],
            ),
            child: Stack(
              children: <Widget>[
                Positioned(
                  child: Container(
                    height: size.height * .2,
                    width: size.width * .5,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.white),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(180))),
                  ),
                  bottom: -50,
                ),
                Positioned(
                  child: Container(
                    height: size.height * .2,
                    width: size.width * .5,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.white),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(180),
                            topRight: Radius.circular(10))),
                  ),
                  bottom: -50,
                  right: 0,
                ),
              ],
            ),
          ),
          Positioned(
            top: size.height * .12,
            bottom: 0,
            left: 0,
            right: 0,
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    profileImage != null ?
                    Align(
                      alignment: Alignment.center,
                      child: Stack(
                        children: <Widget>[
                          MyCircleAvatar(
                              radius: 50,
                              fileImage: FileImage(File(profileImage))
                          ),
                        ],
                      ),
                    ):
                    Align(
                      alignment: Alignment.center,
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CameraApp(
                                        sendImagePath: setImagePath,
                                        cropShape: shape.Circle,
                                      )));
                        },
                        child: Stack(
                          children: <Widget>[
                            MyCircleAvatar(
                              radius: 50,
                              assetImage: AssetImage("assets/user.png")
                            ),
                             Container(
                                decoration:
                                    BoxDecoration(shape: BoxShape.circle),
                                height: 110,
                                width: 110,
                                child: Icon(
                                  Icons.camera_alt,
                                  size: 60,
                                  color: Colors.grey.withOpacity(.7),
                                ))
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Form(
                        child: ListView(
                          shrinkWrap: true,
                          padding: EdgeInsets.only(
                              left: 20, right: 20, top: 0, bottom: 10),
                          children: <Widget>[
                            Container(
                              child: Text(
                                "Full Name",
                              ),
                              padding: EdgeInsets.all(10),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.grey[100]),
                              child: Text(
                                username,
                              ),
                              padding: EdgeInsets.all(20),
                            ),
                            Container(
                              child: Text(
                                "Phone Number",
                              ),
                              padding: EdgeInsets.all(10),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.grey[100]),
                              child: Text(
                                phone,
                              ),
                              padding: EdgeInsets.all(20),
                            ),
                            Container(
                              child: Text(
                                "Designation",
                              ),
                              padding: EdgeInsets.all(10),
                            ),
                            isUpdating
                                ? Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey[100]),
                                    padding: EdgeInsets.only(
                                        left: 20, top: 5, bottom: 5, right: 20),
                                    child: TextField(
                                      minLines: 1,
                                      maxLines: 3,
                                      controller: _designationController,
                                      decoration: InputDecoration(
                                          contentPadding: EdgeInsets.only(
                                              right: 10, top: 5, bottom: 5),
                                          hintText: "Enter Designation",
                                          border: InputBorder.none),
                                    ),
                                  )
                                : Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey[100]),
                                    child: Text(
                                      "Software Engineer",
                                    ),
                                    padding: EdgeInsets.all(20),
                                  ),
                            Container(
                              child: Text(
                                "Family Member",
                              ),
                              padding: EdgeInsets.all(10),
                            ),
                            isUpdating
                                ? Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey[100]),
                                    padding: EdgeInsets.only(
                                        left: 20, bottom: 5, top: 5, right: 20),
                                    child: TextField(
                                      minLines: 1,
                                      maxLines: 3,
                                      controller: _familyMembersController,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                          contentPadding: EdgeInsets.only(
                                              right: 10, top: 5, bottom: 5),
                                          hintText: "Enter Family Member",
                                          border: InputBorder.none),
                                    ),
                                  )
                                : Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey[100]),
                                    child: Text(
                                      "5",
                                    ),
                                    padding: EdgeInsets.all(20),
                                  ),
                            Container(
                              child: Text(
                                "Family Members Occupations",
                              ),
                              padding: EdgeInsets.all(10),
                            ),
                            isUpdating
                                ? Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey[100]),
                                    padding: EdgeInsets.only(
                                        left: 20, bottom: 5, top: 5, right: 20),
                                    child: TextField(
                                      minLines: 1,
                                      maxLines: 3,
                                      controller: _familyMembersOccController,
                                      keyboardType: TextInputType.text,
                                      decoration: InputDecoration(
                                          contentPadding: EdgeInsets.only(
                                              right: 10, top: 5, bottom: 5),
                                          hintText:
                                              "Enter Engineer, Doctor... ",
                                          border: InputBorder.none),
                                    ),
                                  )
                                : Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey[100]),
                                    child: Text(
                                      "5",
                                    ),
                                    padding: EdgeInsets.all(20),
                                  ),
                            Container(
                              child: Text(
                                "NID",
                              ),
                              padding: EdgeInsets.all(10),
                            ),
                            isUpdating
                                ? RaisedButton.icon(
                                    padding: EdgeInsets.all(10),
                                    color: Colors.grey[300],
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        side:
                                            BorderSide(color: Colors.blueGrey)),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => CameraApp(
                                                    sendImagePath: setImagePath,
                                                    cropShape: shape.Rectangle,
                                                  )));
                                    },
                                    icon: Icon(Icons.camera),
                                    label: Text("Capture NID"))
                                : Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.grey[100]),
                                    child: Text(
                                      "4569853695002",
                                    ),
                                    padding: EdgeInsets.all(20),
                                  ),
                            SizedBox(
                              height: 10,
                            ),
                            !isUpdating
                                ? Container()
                                : FlatButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        side:
                                            BorderSide(color: Colors.blueGrey)),
                                    padding: EdgeInsets.all(15),
                                    color: Colors.blueGrey[900],
                                    onPressed: () {
                                      saveProfile();
                                    },
                                    child: Text(
                                      "Update",
                                      style: TextStyle(color: Colors.white),
                                    ))
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  saveProfile() async {
    var des = _designationController.text;
    var fam = _familyMembersController.text;
    var famO = _familyMembersOccController.text;

    var data = await restService.updateProfile(
        userId: userId,
        fMembers: fam,
        mOccupation: famO,
        userProfession: des,
        profileImg: images[0],
        nidFront: images[1],
        nidBack: images[2]);
    if (!data.error) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage()));
      print("success");
    }
  }
}
