import 'dart:io';

import 'package:House/model/ImageCropeShape.dart';
import 'package:House/model/user_profile.dart';
import 'package:House/plugin/camera.dart';
import 'package:House/plugin/expansion_section.dart';
import 'package:House/plugin/mycircleavater.dart';
import 'package:House/service/rest_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileSettings extends StatefulWidget {
  final Function isBottomShow;

  ProfileSettings({Key key, this.isBottomShow}) : super(key: key);

  @override
  _ProfileSettingsState createState() => _ProfileSettingsState();
}

class _ProfileSettingsState extends State<ProfileSettings> {
  RestService get restService => GetIt.I<RestService>();
  String _designation, _familyMember, _nid;

  TextEditingController _designationController = TextEditingController(),
      _familyMembersController = TextEditingController(),
      _familyMembersOccController = TextEditingController();

  Size size;
  bool isUpdating = true;
  String _extractText = 'Unknown';

  var images = [];
  bool isProfileSet = false;

  var userId, userType;
  bool isExpand = false;

  String profileImage;

  UserProfile _profile;

  @override
  void initState() {
    super.initState();
    getUserInfo();
  }

  getUserProfile(userId) async{
    var data = await restService.getProfile(id: userId);
    if(!data.error){
      if(!mounted) return;
      setState(() {
        _profile = data.data;
      });
    }
  }

  getUserInfo() async {
    final prefs = await SharedPreferences.getInstance();
    userId = prefs.getString('userId');
    userType = prefs.getString("userType");
    await getUserProfile(userId);
  }

  setImagePath(path, isProfile) {
    if(!mounted) return;
    setState(() {
      if (isProfile) {
        isProfileSet = true;
        profileImage = path;
        images.insert(0, path);
      } else {
        images.add(path);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: Colors.white,
          ),
          Container(
            height: size.height * .25,
            decoration: BoxDecoration(
              color: Colors.blueGrey[900],
            ),
            child: Stack(
              children: <Widget>[
                Positioned(
                  child: Container(
                    height: size.height * .2,
                    width: size.width * .5,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.white),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(180))),
                  ),
                  bottom: -50,
                ),
                Positioned(
                  child: Container(
                    height: size.height * .2,
                    width: size.width * .5,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.white),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(180),
                            topRight: Radius.circular(10))),
                  ),
                  bottom: -50,
                  right: 0,
                ),
              ],
            ),
          ),
          Positioned(
            top: size.height * .12,
            bottom: 0,
            left: 0,
            right: 0,
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: MyCircleAvatar(
                          radius: 50,
                          assetImage: _profile == null? AssetImage("assets/user.png") : null,
                        networkImage: _profile != null ? NetworkImage(_profile.result.userImage) : null,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Expanded(
                      child: ListView(
                        shrinkWrap: true,
                        padding: EdgeInsets.only(
                            left: 20, right: 20, top: 0, bottom: 10),
                        children: <Widget>[
                          SizedBox(
                            height: 10,
                          ),
                          _profile == null ? Container() :
                          Container(
                            child: ExpansionTile(
                              title: Text("Profile"),
                              leading: Icon(Icons.account_circle),
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Full Name",
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  width: size.width,
                                  margin: EdgeInsets.only(top: 5, bottom: 0, left: 10, right: 10),
                                  padding: EdgeInsets.all(10),
                                ),
                                Container(
                                  width: size.width,
                                  margin: EdgeInsets.only(top: 5, bottom: 0, left: 10, right: 10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.grey[100]),
                                  child: Text(
                                    _profile.result.name,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),
                                Container(
                                  width: size.width,
                                  margin: EdgeInsets.only(top: 5, bottom: 0, left: 10, right: 10),
                                  child: Text(
                                    "Phone Number",
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),
                                Container(
                                  width: size.width,
                                  margin: EdgeInsets.only(top: 5, bottom: 0, left: 10, right: 10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.grey[100]),
                                  child: Text(
                                    _profile.result.mobile,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),
                                Container(
                                  width: size.width,
                                  margin: EdgeInsets.only(top: 5, bottom: 0, left: 10, right: 10),
                                  child: Text(
                                    "Designation",
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),
                                Container(
                                  width: size.width,
                                  margin: EdgeInsets.only(top: 5, bottom: 0, left: 10, right: 10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.grey[100]),
                                  child: Text(
                                    _profile.result.designation,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),
                                Container(
                                  width: size.width,
                                  margin: EdgeInsets.only(top: 5, bottom: 0, left: 10, right: 10),
                                  child: Text(
                                    "Total Family Member's",
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),
                                Container(
                                  width: size.width,
                                  margin: EdgeInsets.only(top: 5, bottom: 0, left: 10, right: 10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.grey[100]),
                                  child: Text(
                                    _profile.result.familymembers,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),
                                Container(
                                  width: size.width,
                                  margin: EdgeInsets.only(top: 5, bottom: 0, left: 10, right: 10),
                                  child: Text(
                                    "Members Occupation's",
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),
                                Container(
                                  width: size.width,
                                  margin: EdgeInsets.only(top: 5, bottom: 0, left: 10, right: 10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.grey[100]),
                                  child: Text(
                                    _profile.result.occupationOfMembers,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),
//                                Container(
//                                  width: size.width,
//                                  margin: EdgeInsets.only(top: 5, bottom: 0, left: 10, right: 10),
//                                  child: Text(
//                                    "Designation",
//                                    style: TextStyle(fontSize: 12),
//                                  ),
//                                  padding: EdgeInsets.all(10),
//                                ),
//                                Container(
//                                  width: size.width,
//                                  margin: EdgeInsets.only(top: 5, bottom: 0, left: 10, right: 10),
//                                  decoration: BoxDecoration(
//                                      borderRadius: BorderRadius.circular(10),
//                                      color: Colors.grey[100]),
//                                  child: Text(
//                                    "Software Engineer",
//                                    style: TextStyle(fontSize: 12),
//                                  ),
//                                  padding: EdgeInsets.all(10),
//                                ),
                                SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: Colors.grey)
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            child: ExpansionTile(
                              title: Text("Language"),
                              leading: Icon(Icons.translate),
                              children: <Widget>[
                                Wrap(
                                  children: <Widget>[
                                    Chip(
                                      label: Text("English"),
                                      backgroundColor: Colors.grey[50],
                                      elevation: 5,
                                    ),
                                    Chip(
                                      label: Text("বাংলা"),
                                      backgroundColor: Colors.grey[50],
                                      elevation: 5,
                                    )
                                  ],
                                  spacing: 5,
                                )
                              ],
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(color: Colors.grey)
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
