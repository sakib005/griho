import 'package:House/model/privacy.dart';
import 'package:flutter/material.dart';

class PrivacyPolicy extends StatefulWidget {

  final Privacy privacy;
  final int selected;
  final Function onSelected;
  PrivacyPolicy({Key key, this.privacy, this.selected, this.onSelected}): super(key: key);

  @override
  _PrivacyPolicyState createState() => _PrivacyPolicyState();
}

class _PrivacyPolicyState extends State<PrivacyPolicy> {

  String _radioValue; //Initial definition of radio button value
  String choice;

  void radioButtonChanges(String value) {
    setState(() {
      _radioValue = value;
      var gg = widget.privacy.result.firstWhere((element) => element.key == int.parse(value));
      widget.onSelected(gg.key);
      choice = gg.key.toString();
      debugPrint(choice); //Debug the choice in console
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _radioValue = widget.selected.toString();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Select Privacy"),
          backgroundColor: Colors.white,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 20,left: 10, bottom: 5),
                child: Text("Who can see your post?", style: TextStyle(color: Colors.blueGrey[900], fontWeight: FontWeight.bold),)),
            Container(
                padding: EdgeInsets.only(left: 10),
                child: Text("Your post will appear Notice Board.", style: TextStyle(color: Colors.blueGrey[900],),)),
            SizedBox(
              height: 15,
            ),
            Column(
              children: widget.privacy.result.map((e){
                return GestureDetector(
                  onTap: (){
                    setState(() {
                      _radioValue = e.key.toString();
                      widget.onSelected(e.key);
                      print(_radioValue);
                    });
                  },
                  child: Column(
                    children: <Widget>[
                      Container(
                        color: Colors.grey[50],
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Radio(
                                value: e.key.toString(),
                                groupValue: _radioValue,
                                onChanged: radioButtonChanges,
                              focusColor: Colors.blue,
                              activeColor: Colors.blue,
                            ),
                            e.key == 2?
                            Icon(Icons.security, size: 23,):
                            Image.asset("assets/gloab.png", width: 20,),
                            SizedBox(
                              width: 15,
                            ),
                            Column(
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    e.value, style: TextStyle(color: Colors.blueGrey[700], fontSize: 13),
                                  ),
                                ),
                                e.key == 2?
                                    Text("Only owner can see this post", style: TextStyle(fontSize: 12),):
                                    Text("Every one can see this post", style: TextStyle(fontSize: 12),)
                              ],
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                            ),
                          ],
                        ),
                      ),
                      Divider(
                        color: Colors.blueGrey,
                      )
                    ],
                  ),
                );
              }).toList(),
            )
          ],
        ),
      ),
    );
  }
}
