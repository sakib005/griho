import 'package:House/model/notice.dart';
import 'package:House/plugin/expansion_section.dart';
import 'package:House/plugin/mycircleavater.dart';
import 'package:House/service/rest_service.dart';
import 'package:House/style_guid/color_guid.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';

class NotificationView extends StatefulWidget {

  final String userId;
  final String houseId;
  final String postId;

  NotificationView({Key key, this.userId, this.houseId, this.postId}): super(key: key);

  @override
  _NotificationViewState createState() => _NotificationViewState();
}

class _NotificationViewState extends State<NotificationView> {

  RestService get restService => GetIt.I<RestService>();
  final GlobalKey<FormState> _kayState = GlobalKey<FormState>();
  TextEditingController _controller = new TextEditingController();

  ScrollController _scrollController;
  bool isClick = false;

  NoticeResult notice;

  @override
  void initState() {
    super.initState();
    getPost();
  }

  void _toEnd() {
    _scrollController.animateTo(
      _scrollController.position.maxScrollExtent,
      duration: const Duration(milliseconds: 500),
      curve: Curves.ease,
    );
  }

  void postIssue() async {
    var formState = _kayState.currentState;
    if (formState.validate()) {
      formState.save();

      if (!isClick) {
        var tt = _controller.text;
        _controller.text = "";
        final result =
        await restService.postReply(id: widget.userId,
            houseid: widget.houseId,
            message: tt,
            privacyType: notice.privacyId,
            parentId: notice.messageId.toString());
        if (!result.error) {
          setState(() {
            notice.listOfSubMessages.add(ListOfSubMessages(
                apartmentNo: notice.apartmentNo,
                message: tt,
                messageId: 1,
                postedTime: DateTime.now().toString(),
                privacyId: notice.privacyId,
                userId: widget.userId.toString(),
                userImage: notice.userImage,
                userName: notice.userName
            ));
            isClick = false;
            notice.count += 1;
            _toEnd();
          });
        }
      }
    }
  }

  getPost() async {
    var data = await restService.getSinglePost(
        userId: widget.userId, houseid: widget.houseId, postId: widget.postId);
    if (!data.error) {
      setState(() {
        if(data.data.result.length > 0){
          notice = data.data.result.first;
        }
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Notification", style: TextStyle(color: Colors.white),),
          backgroundColor: Colors.blueGrey[800],
          iconTheme: IconThemeData(color: Colors.white),
        ),
        body: notice == null ? Container() :
        Container(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 5),
                child: Row(
                  children: <Widget>[
                    MyCircleAvatar(
                      radius: 20,
                      networkImage: NetworkImage(notice.userImage),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(notice.userName, style: TextStyle(color: Colors
                            .black, fontSize: 12, fontWeight: FontWeight
                            .bold),),
                        Text(new DateFormat.yMMMd().format(DateTime.parse(
                            notice.postedTime)) + " at " + new DateFormat
                            .jm().format(DateTime.parse(notice.postedTime),),
                          style: TextStyle(color: Colors.grey, fontSize: 11),)
                      ],
                    ),
                    Expanded(
                      child: Align(
                          alignment: Alignment.centerRight,
                          child: Container(
                              padding: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                  color: Colors.pink[900],
                                  shape: BoxShape.circle
                              ),
                              child: Text(notice.apartmentNo,
                                style: TextStyle(fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontSize: 10),))),
                    ),
                    InkWell(
                        onTap: () {
                          deletePost(notice.messageId.toString());
                        },
                        child: Icon(Icons.more_vert)),
                    SizedBox(
                      width: 10,
                    )
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: Container(
                  padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[50],
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Column(
                    children: <Widget>[
                      Text(notice.message == null ? "" : notice.message,
                        style: TextStyle(
                            color: Colors.grey[600]
                        ),),
                      SizedBox(
                        height: 25,
                      ),
                      Row(
                        children: <Widget>[
                          Image.asset(
                            "assets/chat.png", width: 20, color: Colors
                              .grey[400],),
                          SizedBox(width: 10,),
                          Text(notice.count.toString(), style: TextStyle(
                              color: Colors.grey[600]),)
                        ],
                      ),
                      ExpandedSection(
                          expand: true,
                          child: Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Container(
                              child: Column(
                                children: <Widget>[
                                  Divider(color: Colors.grey,),
                                  Expanded(child: ListView(
                                    padding: EdgeInsets.only(top: 0),
                                    shrinkWrap: true,
                                    controller: _scrollController,
                                    children: notice.listOfSubMessages.map((
                                        e) {
                                      return widget.userId != null &&
                                          int.parse(e.userId) ==
                                              int.parse(widget.userId) ?
                                      Container(
                                        margin: EdgeInsets.only(
                                            bottom: 10, top: 5),
                                        child: Container(
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment
                                                .end,
                                            children: [
                                              Text(
                                                new DateFormat.jm().format(
                                                    DateTime.parse(
                                                        e.postedTime)),
                                                style: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .bodyText1
                                                    .apply(color: Colors.grey),
                                              ),
                                              SizedBox(width: 15),
                                              Container(
                                                constraints: BoxConstraints(
                                                    maxWidth: MediaQuery
                                                        .of(context)
                                                        .size
                                                        .width * .5),
                                                padding: const EdgeInsets.all(
                                                    15.0),
                                                decoration: BoxDecoration(
                                                  color: Colors.green[400],
                                                  borderRadius: BorderRadius
                                                      .only(
                                                    topRight: Radius.circular(
                                                        25),
                                                    bottomLeft: Radius.circular(
                                                        25),
                                                    topLeft: Radius.circular(
                                                        25),
                                                  ),
                                                ),
                                                child: Text(
                                                  e.message,
                                                  style: Theme
                                                      .of(context)
                                                      .textTheme
                                                      .bodyText2
                                                      .apply(
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ) :
                                      Container(
                                        margin: EdgeInsets.only(
                                            bottom: 10, top: 5),
                                        child: Row(
                                          children: [
                                            Align(
                                              child: MyCircleAvatar(
                                                networkImage: NetworkImage(
                                                    e.userImage),
                                                radius: 20,
                                              ),
                                              alignment: Alignment.topLeft,
                                            ),
                                            Column(
                                              crossAxisAlignment: CrossAxisAlignment
                                                  .start,
                                              children: [
                                                Text(
                                                  e.userName,
                                                  style: Theme
                                                      .of(context)
                                                      .textTheme
                                                      .caption,
                                                ),
                                                Container(
                                                  constraints: BoxConstraints(
                                                      maxWidth: MediaQuery
                                                          .of(context)
                                                          .size
                                                          .width * .4),
                                                  padding: const EdgeInsets.all(
                                                      15.0),
                                                  decoration: BoxDecoration(
                                                    color: backgroundColor,
                                                    borderRadius: BorderRadius
                                                        .only(
                                                      topRight: Radius.circular(
                                                          25),
                                                      bottomLeft: Radius
                                                          .circular(25),
                                                      bottomRight: Radius
                                                          .circular(25),
                                                    ),
                                                  ),
                                                  child: Text(
                                                    e.message,
                                                    style: Theme
                                                        .of(context)
                                                        .textTheme
                                                        .bodyText2
                                                        .apply(
                                                      color: Colors.black87,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(width: 15),
                                            Text(
                                              new DateFormat.jm().format(
                                                  DateTime.parse(e.postedTime)),
                                              style: Theme
                                                  .of(context)
                                                  .textTheme
                                                  .bodyText1
                                                  .apply(color: Colors.grey),
                                            ),
                                          ],
                                        ),
                                      );
                                    }).toList(),
                                  )),
                                  Container(
                                    padding: EdgeInsets.all(5),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius
                                                  .circular(35.0),
                                              boxShadow: [
                                                BoxShadow(
                                                    offset: Offset(0, 3),
                                                    blurRadius: 5,
                                                    color: Colors.grey)
                                              ],
                                            ),
                                            child: Row(
                                              children: [
                                                IconButton(
                                                    icon: Icon(Icons.face),
                                                    onPressed: () {}),
                                                Expanded(
                                                  child: Form(
                                                    key: _kayState,
                                                    child: TextField(
                                                      minLines: 1,
                                                      maxLines: 3,
                                                      controller: _controller,
                                                      style: TextStyle(
                                                          fontSize: 13),
                                                      decoration: InputDecoration(
                                                        contentPadding: EdgeInsets
                                                            .only(right: 10,
                                                            top: 5,
                                                            bottom: 5),
                                                        hintText: "Reply..",
                                                        border: InputBorder
                                                            .none,),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        SizedBox(width: 15),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              postIssue();
                                            });
                                          },
                                          child: Container(
                                            padding: const EdgeInsets.all(15.0),
                                            decoration: BoxDecoration(
                                                color: Colors.green[400],
                                                shape: BoxShape.circle),
                                            child: Icon(
                                              Icons.send,
                                              color: Colors.white,
                                              size: 15,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  deletePost(messageId) async {
    var data = await restService.deletePost(
        userId: widget.userId, houseid: widget.houseId, messageId: messageId);
    if (!data.error) {}
  }
}
