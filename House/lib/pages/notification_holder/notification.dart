import 'package:House/model/notification_list.dart';
import 'package:House/pages/notification_holder/notification_view.dart';
import 'package:House/plugin/mycircleavater.dart';
import 'package:House/service/rest_service.dart';
import 'package:House/style_guid/app_background.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificationActivity extends StatefulWidget {
  @override
  _NotificationActivityState createState() => _NotificationActivityState();
}

class _NotificationActivityState extends State<NotificationActivity> {
  RestService get restService => GetIt.I<RestService>();

  NotificationList notification;
  String houseId, userId;
  getUserInfo() async{
    final prefs = await SharedPreferences.getInstance();
    userId =  prefs.getString('userId');
    houseId =  prefs.getString('houseId');
    getNotificationList();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   getUserInfo();
  }

  getNotificationList() async {
    var data = await restService.getNotificationList(
        id: userId, houseid: houseId, page: 1);
    if (!data.error) {
      setState(() {
        notification = data.data;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()=> SystemNavigator.pop(),
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            AppBackground(),
            Container(
              margin: EdgeInsets.only(top: 80),
              child: notification == null
                  ? Container()
                  : ListView(
                padding: EdgeInsets.only(top: 0),
                      children: notification.result.map((e) {
                        return GestureDetector(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> NotificationView(userId: userId, houseId: houseId, postId: e.messageId.toString(),)));
                          },
                          child: Container(
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(color: Colors.grey[50]),
                              child: Row(
                                children: <Widget>[
                                  MyCircleAvatar(
                                    networkImage: NetworkImage(e.userImage),
                                    radius: 25,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          e.userName,
                                          style: TextStyle(
                                              color: Colors.grey[900], fontSize: 12),
                                        ),
                                        SizedBox(
                                          height: 3,
                                        ),
                                        Text(
                                          e.message,
                                          style: TextStyle(color: Colors.grey[600], fontSize: 12),
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        Text(new DateFormat.yMMMd().format(DateTime.parse(e.postedTime)) + " at " + new DateFormat.jm().format(DateTime.parse(e.postedTime),),
                                          style: TextStyle(color: Colors.grey, fontSize: 11),)
                                      ],
                                    ),
                                  )
                                ],
                              )),
                        );
                      }).toList(),
                    ),
            )
          ],
        ),
      ),
    );
  }
}
