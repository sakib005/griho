import 'package:House/model/manage_request.dart';
import 'package:House/plugin/mycircleavater.dart';
import 'package:House/service/rest_service.dart';
import 'package:House/style_guid/app_background.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Manage extends StatefulWidget {
  @override
  _ManageState createState() => _ManageState();
}

class _ManageState extends State<Manage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => SystemNavigator.pop(),
      child: Scaffold(
        body: NoonLoopingDemo(),
      ),
    );
  }
}

class NoonLoopingDemo extends StatefulWidget {
  @override
  _NoonLoopingDemoState createState() => _NoonLoopingDemoState();
}

class _NoonLoopingDemoState extends State<NoonLoopingDemo> {

  RestService get restService => GetIt.I<RestService>();


  ManageRequest request;

  String userId, userType, houseId;
  List<Widget> imageSliders = [];

  @override
  void initState() {
    super.initState();
    getUserInfo();
  }

  getUserInfo() async {
    final prefs = await SharedPreferences.getInstance();
    userId = prefs.getString('userId');
    houseId = prefs.getString("houseId");
    userType = prefs.getString("userType");
    await getManages();
  }

  getManages() async{
    var data = await restService.getManageList(id: userId, houseId: houseId);
    if(!data.error){
      if (!mounted) return;
      setState(() {
        request = data.data;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        child: Stack(
          children: <Widget>[
            AppBackground(),
            Center(
              child: Container(
                  child: CarouselSlider(
                options: CarouselOptions(
                  enlargeCenterPage: true,
                  enableInfiniteScroll: false,
                  aspectRatio: 5,
                  initialPage: 0,
                  height: height * .7,
                  autoPlay: false,
                ),
                items: request == null ? []:
                request.result
                    .map((item) => Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.grey[400]),
                      borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(.6),
                        blurRadius: 7,
                        spreadRadius: 4
                      )
                    ]
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.blueGrey[700],
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: 10,
                            ),
                            Stack(
                              children: <Widget>[
                                Container(
                                  child: Center(
                                    child: MyCircleAvatar(
                                      networkImage: NetworkImage(item.userImage),
                                      radius: 30,
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(top: 5,bottom: 5,right: 10),
                                        padding: EdgeInsets.all(8),
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.pink
                                        ),
                                        child: Text(item.apartment, style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 3,
                            ),
                            Center(
                              child: Text(item.name, style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold
                              ),),
                            ),
                            SizedBox(
                              height: 3,
                            ),
                            Center(
                              child: Text("item.designation", style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                              ),),
                            ),
                            SizedBox(
                              height: 3,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.phone_in_talk, color: Colors.white, size: 20,),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(item.mobile, style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 13,
                                ),),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Divider(
                              height: 2,
                              thickness: 1,
                              color: Colors.grey,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10, top: 5, bottom: 5),
                        child: Text("Other Info", style: TextStyle(
                          color: Colors.blueGrey[800],
                          fontSize: 15,
                        ),),
                      ),
                      Divider(
                        height: 2,
                        thickness: 1,
                        color: Colors.grey,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 10, top: 5),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Icon(Icons.group_add),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              flex: 4,
                              child: Text(
                                "Member's: ",
                                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Expanded(
                              flex: 6,
                              child: Text(
                                item.familyMembers,
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 10, top: 5),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Icon(Icons.card_travel),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: AutoSizeText(
                                "Occupation's: ",
                                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                              ),
                              flex: 4,
                            ),
                            Expanded(
                              child: AutoSizeText(
                                item.designation,
                                style: TextStyle(fontSize: 15),
                                maxLines: 3,
                              ),
                              flex: 6,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 10, top: 5),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Icon(Icons.access_time),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: AutoSizeText(
                                "Time: ",
                                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                              ),
                              flex: 4,
                            ),
                            Expanded(
                              child: AutoSizeText(
                                new DateFormat.yMMMd().format(DateTime.parse(item.requestTime)),
                                style: TextStyle(fontSize: 15),
                                maxLines: 3,
                              ),
                              flex: 6,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 10, top: 5),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Icon(Icons.credit_card),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: AutoSizeText(
                                "NID :",
                                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                              ),
                              flex: 4,
                            ),
                            Expanded(
                              flex: 6,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 10, top: 5, bottom: 5),
                                child: Image.network(item.nidFront, fit: BoxFit.cover,),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 10, top: 5),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Icon(Icons.attach_money),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: AutoSizeText(
                                "Rent: ",
                                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                              ),
                              flex: 4,
                            ),
                            Expanded(
                              child: AutoSizeText(
                                "${item.rent} /-",
                                style: TextStyle(fontSize: 15, color: Colors.blueGrey[800], fontWeight: FontWeight.bold),
                                maxLines: 3,
                              ),
                              flex: 6,
                            ),
                          ],
                        ),
                      ),
                      Expanded(child: SizedBox()),
                      Divider(
                        height: 2,
                        thickness: 1,
                        color: Colors.grey,
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          padding: const EdgeInsets.only(left: 10, top: 5, bottom: 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              FlatButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)
                                ),
                                onPressed: (){
                                  onApprovalSend(isApprove: "0", requestId: item.requestId.toString(), type: item.isOwner);
                                },
                                child: Text("Reject", style: TextStyle(color: Colors.black87),),
                                color: Colors.grey[200],
                              ),
                              SizedBox(width: 10,),
                              FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)
                                ),
                                onPressed: (){
                                  onApprovalSend(isApprove: "1", requestId: item.requestId.toString(), type: item.isOwner);
                                }, child: Text("Approve", style: TextStyle(color: Colors.white),), color: Colors.green[800],),
                              SizedBox(width: 10,),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ))
                    .toList()
              )),
            ),
          ],
        ),
      ),
    );
  }

  onApprovalSend({String isApprove, String requestId, int type})async{
    if(type == 0){
      var data = await restService.requestApproveOrReject(userId: userId, requestId: requestId, isApprove: isApprove);
      if(!data.error){
        if(!mounted) return;
        setState(() {
          request.result.removeWhere((element) => element.requestId == int.parse(requestId));
        });
      }
    }else{
      var data = await restService.requestApproveOrRejectOwner(userId: userId, requestId: requestId, isApprove: isApprove);
      if(!data.error){
        if(!mounted) return;
        setState(() {
          request.result.removeWhere((element) => element.requestId == int.parse(requestId));
        });
      }
    }
  }

}
