import 'package:House/service/rest_service.dart';
import 'package:flutter/material.dart';
import 'package:House/model/building_information.dart';
import 'package:get_it/get_it.dart';

class BuildingDetails extends StatefulWidget {

  final String houseId;

  BuildingDetails({Key key, this.houseId}) : super(key: key);

  @override
  _BuildingDetailsState createState() => _BuildingDetailsState();
}

class _BuildingDetailsState extends State<BuildingDetails> {

RestService get restService => GetIt.I<RestService>();
  BuildingInfo info;

  @override
  void initState() { 
    super.initState();
    getInfo();
  }

  getInfo()async{
    var data = await restService.getHouseInformation(houseId: widget.houseId);
    if(!data.error){
      setState(() {
        info = data.data;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Building Information", style: TextStyle(color: Colors.black),),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Text("Building Name:"),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(info.result.houseName),
                    )
                  ],
                ),
              ),
               Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Text("Building Id:"),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(info.result.houseId),
                    )
                  ],
                ),
              ),
               Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Text("Total Floor:"),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(info.result.totalFloor),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Text("Total Unit:"),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(info.result.totalUnit),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Text("Occupied Flat:"),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(info.result.totalOccupied),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Text("Available Flat:"),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(info.result.totalVacant),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}