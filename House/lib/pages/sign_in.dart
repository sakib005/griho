import 'package:House/pages/sign_up.dart';
import 'package:House/plugin/scale_route.dart';
import 'package:House/service/rest_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:House/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  String _phone, _password;
  final _focus = FocusNode();
  final GlobalKey<FormState> _kayState = GlobalKey<FormState>();
  Size size;
  RestService get restService => GetIt.I<RestService>();

  onSignIn() async {
    var formState = _kayState.currentState;
    if (formState.validate()) {
      formState.save();
      var data =
          await restService.getSignIn(mobile: _phone, password: _password);
      if (!data.error) {
        if (data.data.result == "success") {
          final prefs = await SharedPreferences.getInstance();
          await prefs.setString('userId', data.data.userid.toString());
          await prefs.setString("userType", data.data.usertypeid.toString());
          Navigator.push(context, ScaleRoute(page: MyHomePage()));
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
          body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Stack(
          children: <Widget>[
            Positioned(
                right: -50,
                top: -50,
                child: Image.asset("assets/leaf.png", width: 200)),
            Container(
              padding: EdgeInsets.all(10),
              child: Form(
                  key: _kayState,
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      SizedBox(height: size.height * .2),
                      Center(
                        child: Text(
                          "Login",
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 30,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        margin: EdgeInsets.all(30),
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              // ignore: missing_return
                              validator: (input) {
                                if (input.isEmpty) {
                                  return "Phone Number is required";
                                }
                              },
                              style: TextStyle(color: Colors.black),
                              textInputAction: TextInputAction.next,
                              keyboardType: TextInputType.emailAddress,
                              onSaved: (input) => _phone = input,
                              onFieldSubmitted: (v) {
                                FocusScope.of(context)
                                    .requestFocus(_focus);
                              },
                              cursorColor: Colors.blueGrey[800],
                              decoration: InputDecoration(
                                labelText: "Phone Number",
                                labelStyle: TextStyle(
                                    fontSize: 15,
                                    color: Colors.blueGrey[300]),
                                filled: true,
                                focusColor: Colors.white,
                                focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.circular(5.0),
                                  borderSide: BorderSide(
                                    color: Colors.transparent,
                                  ),
                                ),
                                prefixIcon: Icon(Icons.phone_in_talk,
                                    color: Colors.blueGrey[300]),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.circular(5.0),
                                  borderSide: BorderSide(
                                    color: Colors.transparent,
                                    width: 2.0,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 10),
                            TextFormField(
                              // ignore: missing_return
                              validator: (input) {
                                if (input.isEmpty || input.length < 6) {
                                  return "Password can't be less then 6 disigt";
                                }
                              },
                              style: TextStyle(color: Colors.black),
                              focusNode: _focus,
                              textInputAction: TextInputAction.done,
                              keyboardType: TextInputType.text,
                              obscureText: true,
                              onSaved: (input) => _password = input,
                              cursorColor: Colors.blueGrey[800],
                              decoration: InputDecoration(
                                labelText: "Password",
                                labelStyle: TextStyle(
                                    fontSize: 15,
                                    color: Colors.blueGrey[300]),
                                filled: true,
                                focusColor: Colors.white,
                                focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.circular(5.0),
                                  borderSide: BorderSide(
                                    color: Colors.transparent,
                                  ),
                                ),
                                prefixIcon: Icon(
                                    Icons.screen_lock_portrait,
                                    color: Colors.blueGrey[300]),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.circular(5.0),
                                  borderSide: BorderSide(
                                    color: Colors.transparent,
                                    width: 2.0,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 30),
                            Container(
                                margin: EdgeInsets.all(5),
                                alignment: FractionalOffset.center,
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.center,
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        child: GestureDetector(
                                          onTap: () {},
                                          child: Text(
                                            "Forgot Password",
                                            style: TextStyle(
                                              color:
                                                  Colors.blueGrey[200],
                                            ),
                                            textAlign: TextAlign.end,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                )),
                            SizedBox(height: 20),
                            GestureDetector(
                              onTap: () {
                                onSignIn();
                              },
                              child: Container(
                                width: 120,
                                decoration: BoxDecoration(
                                    color: Colors.green[800],
                                    borderRadius:
                                        BorderRadius.circular(20),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.green
                                            .withOpacity(0.5),
                                        spreadRadius: 1,
                                        blurRadius: 4,
                                        offset: Offset(0, 3),
                                      )
                                    ]),
                                padding: EdgeInsets.all(10),
                                child: Center(
                                    child: Text(
                                  "SIGN IN",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                )),
                              ),
                            ),
                            SizedBox(
                              height: size.height*.2,
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                margin: EdgeInsets.only(left: 20),
                                child: Row(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "Your First Time?",
                                      style: TextStyle(
                                          color: Colors.blueGrey[400],
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(context,
                                            ScaleRoute(page: SignUp()));
                                      },
                                      child: Text(
                                        "Sign Up",
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.grey,
                                            fontWeight:
                                                FontWeight.bold),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  )),
            ),
          ],
        ),
      )),
    );
  }
}
