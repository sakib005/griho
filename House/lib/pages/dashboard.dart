import 'dart:async';

import 'package:House/model/floor.dart';
import 'package:House/model/notice.dart';
import 'package:House/pages/building_info.dart';
import 'package:House/pages/notice_post/owner_post.dart';
import 'package:House/pages/notice_post/post.dart';
import 'package:House/plugin/ExpansionCard.dart';
import 'package:House/plugin/flat_dialog.dart';
import 'package:House/plugin/scale_route.dart';
import 'package:House/service/rest_service.dart';
import 'package:House/style_guid/app_background.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:House/pages/building_details.dart';

class DashboardActivity extends StatefulWidget {
  DashboardActivity({Key key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<DashboardActivity> {
  RestService get restService => GetIt.I<RestService>();
  final controller= PageController(viewportFraction: 1);

  ScrollController _controllers = ScrollController();
  bool isProgress = true;
  var notice =List<NoticeResult>();
  String userId, userType, houseId;
  Size size;
  int page = 1;
  var building = Floor();
  bool isLoading = true;

  int currentPage = 0;
  bool isSuperAdmin = false;

  getBuildings() async {
    var data = await restService.getFloorInfo(id: userId, houseid: "0");
    if (!data.error) {
      if(data.data.result.length > 0){
        await getNoticeBoard();
      }
       if (!mounted) return;
        setState(() {
          building = data.data;
          isLoading = false;
        });
    }
  }

  getNoticeBoard() async {
    Timer(Duration(milliseconds: 300), () async{
      if(building.result.length > 0){
        houseId = building.result.elementAt(currentPage).houseId.toString();
        isSuperAdmin = int.parse(building.result.elementAt(currentPage).isSuperOwner) == 1;
        print(isSuperAdmin);
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString("houseId", houseId);
        print("$houseId");
        var data = await restService.getNotice(id: userId, houseid: houseId, page: page);
        if (!data.error) {
          if (!mounted) return;
          setState(() {
            isProgress = false;
            if(data.data.result.length > 0){
              data.data.result.forEach((element) {
                notice.add(element);
              });
            }else{
              page == 1 ? page = 1 : page -= 1;
            }
          });
        }
      }
    });
  }

  updateNoticeBoard() async{
    notice.clear();
    getNoticeBoard();
  }

  getUserInfo() async {
    final prefs = await SharedPreferences.getInstance();
    userId = prefs.getString('userId');
    print(userId);
    userType = prefs.getString("userType");
    getBuildings();
  }

  void scrollListener() {
    print(controller.page);
  }

  bool isStackClick = false;

  @override
  void initState() {
    super.initState();
    getUserInfo();
    _controllers..addListener(_scrollListener);
  }

  @override
  void dispose() {
    _controllers.removeListener(_scrollListener);
    super.dispose();
  }

  void _scrollListener() {
    if (_controllers.position.pixels == _controllers.position.maxScrollExtent) {
      if (!mounted) return;
      setState(() {
        isProgress = true;
        page += 1;
        getNoticeBoard();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () => SystemNavigator.pop(),
      child: Scaffold(
          body: GestureDetector(
        onTap: () {
          setState(() {
            if (isStackClick) {
              isStackClick = false;
            } else {
              isStackClick = true;
            }
          });
        },
        child: Stack(
          children: <Widget>[
            isLoading ?
            Container():
             Container(
              child:
              Stack(
                children: <Widget>[
                  AppBackground(),
                  building.result == null ? Container() :
                  Container(
                    child: PageView(
                      controller: controller,
                      onPageChanged: (p){
                        setState(() {
                          currentPage = p;
                          notice.clear();
                          page = 1;
                          getNoticeBoard();
                        });
                      },
                      children: building.result.map((e) {
                        return BuildingInfo(floor: e,);
                      }).toList(),
                    ),
                  ),
                  building.result == null ? Container() :
                  Positioned(
                    bottom: 100,
                    child: building.result.length > 0 ?  Container(
                      width: size.width,
                      child: Center(
                        child: SmoothPageIndicator(
                          controller: controller,
                          count: building.result.length,
                          effect: WormEffect(),
                        ),
                      ),
                    ) : Container(),
                  ),
                  building.result == null || building.result.length != 0 ? Container():
                  Align(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                            padding: EdgeInsets.all(10),
                            child: Image.asset(
                              "assets/stack.png",
                              width: 120,
                              color: Colors.grey[500],
                            )),
                        InkWell(
                          onTap: () {
                            setState(() {
                              if (isStackClick) {
                                isStackClick = false;
                              } else {
                                isStackClick = true;
                              }
                            });
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                color: Colors.pink[800],
                                borderRadius: BorderRadius.circular(20)
                              ),
                              child: Text("Create/Add Building", style: TextStyle(
                                color: Colors.white,
                                fontSize: 15
                              ),)),
                        )
                      ],
                    ),
                  ),
                  building.result.length == 0 ? Container():
                  Positioned(
                    top: 70,
                    right: 10,
                    height: 45,
                    width: 45,
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          if (isStackClick) {
                            isStackClick = false;
                          } else {
                            isStackClick = true;
                          }
                        });
                      },
                      child: Container(
                          decoration: BoxDecoration(
                              color: Colors.grey[200], shape: BoxShape.circle),
                          padding: EdgeInsets.all(10),
                          child: Image.asset(
                            "assets/stack.png",
                            width: 25,
                          )),
                    ),
                  ),
                  Positioned(
                    top: 120,
                    right: 10,
                    height: 45,
                    width: 45,
                    child: InkWell(
                      onTap: () {
                        Navigator.push(context,
                         MaterialPageRoute(builder: (context)=> BuildingDetails(houseId: houseId,)));
                      },
                      child: Container(
                          decoration: BoxDecoration(
                              color: Colors.grey[200], shape: BoxShape.circle),
                          padding: EdgeInsets.all(10),
                          child: Image.asset(
                            "assets/information.png",
                            width: 25,
                          )),
                    ),
                  ),
                  building.result.length == 0 && isStackClick
                      ? Center(
                        child: Container(
                          width: size.width*.7,
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              color: Colors.blueGrey[800],
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(10),
                                  bottomLeft: Radius.circular(10))),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Container(
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      isStackClick = false;
                                    });
                                    showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (BuildContext context) =>
                                            FlatDialog(
                                              profileUpdate: getBuildings,
                                              isAdd: false,
                                              isCreate: true,
                                              isUpdate: false,
                                            ));
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        top: 10, bottom: 5, left: 10, right: 10),
                                    padding: EdgeInsets.all(10),
                                    child: Center(
                                        child: Text(
                                          "Create New Building",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold),
                                        )),
                                    decoration: BoxDecoration(
                                        color: Colors.blueGrey[300],
                                        borderRadius: BorderRadius.circular(5)),
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    isStackClick = false;
                                  });
                                  showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (BuildContext context) =>
                                          FlatDialog(
                                            profileUpdate: getBuildings,
                                            isAdd: true,
                                            isCreate: false,
                                            isUpdate: false,
                                          ));
                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                      top: 5, bottom: 5, left: 10, right: 10),
                                  padding: EdgeInsets.all(10),
                                  child: Center(
                                      child: Text(
                                        "Add Existing Building",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      )),
                                  decoration: BoxDecoration(
                                      color: Colors.blueGrey[300],
                                      borderRadius: BorderRadius.circular(5)),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    if (isStackClick) {
                                      isStackClick = false;
                                    } else {
                                      isStackClick = true;
                                    }
                                  });
                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                      top: 10, bottom: 5, left: 10, right: 10),
                                  padding: EdgeInsets.all(10),
                                  child: Center(
                                      child: Text(
                                        "Close",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      )),
                                  decoration: BoxDecoration(
                                      color: Colors.blue,
                                      borderRadius: BorderRadius.circular(5)),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                      : Container(),
                  building.result.length != 0 && building.result.length > 0 && isStackClick
                      ? Positioned(
                      top: 65,
                      right: 65,
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                bottomRight: Radius.circular(10),
                                bottomLeft: Radius.circular(10))),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    isStackClick = false;
                                    showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (BuildContext context) =>
                                            FlatDialog(
                                              profileUpdate: getBuildings,
                                              isAdd: false,
                                              isCreate: true,
                                              isUpdate: false,
                                            ));
                                  });
                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                      top: 10, bottom: 5, left: 10, right: 10),
                                  width: 160,
                                  padding: EdgeInsets.all(10),
                                  child: Center(
                                      child: Text(
                                        "Create New Building",
                                        style: TextStyle(
                                            color: Colors.blueGrey[900],
                                            fontWeight: FontWeight.bold),
                                      )),
                                  decoration: BoxDecoration(
                                      color: Colors.grey[50],
                                      borderRadius: BorderRadius.circular(5)),
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  isStackClick = false;
                                  showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (BuildContext context) =>
                                          FlatDialog(
                                            profileUpdate: getBuildings,
                                            isAdd: true,
                                            isCreate: false,
                                            isUpdate: false,
                                          ));
                                });
                              },
                              child: Container(
                                margin: EdgeInsets.only(top: 5, bottom: 5),
                                width: 160,
                                padding: EdgeInsets.all(10),
                                child: Center(
                                    child: Text(
                                      "Add Existing Building",
                                      style: TextStyle(
                                          color: Colors.blueGrey[900],
                                          fontWeight: FontWeight.bold),
                                    )),
                                decoration: BoxDecoration(
                                    color: Colors.grey[50],
                                    borderRadius: BorderRadius.circular(5)),
                              ),
                            ),
                            isSuperAdmin ? InkWell(
                              onTap: () {
                                setState(() {
                                  isStackClick = false;
                                  showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (BuildContext context) =>
                                          FlatDialog(
                                            profileUpdate: getBuildings,
                                            isAdd: false,
                                            isCreate: false,
                                            isUpdate: true,
                                            houseId: houseId,
                                          ));
                                });
                              },
                              child: Container(
                                margin: EdgeInsets.only(top: 5, bottom: 10),
                                width: 160,
                                padding: EdgeInsets.all(10),
                                child: Center(
                                    child: Text(
                                      "Update Building",
                                      style: TextStyle(
                                          color: Colors.blueGrey[900],
                                          fontWeight: FontWeight.bold),
                                    )),
                                decoration: BoxDecoration(
                                    color: Colors.grey[50],
                                    borderRadius: BorderRadius.circular(5)),
                              ),
                            ): Container(),
                          ],
                        ),
                      ))
                      : Container(),
                ],
              ),
            ),
            DraggableScrollableSheet(
              initialChildSize: 0.12,
              maxChildSize: 0.9,
              minChildSize: 0.12,
              expand: true,
              builder: (context, scrollController) {
                return SingleChildScrollView(
                  controller: scrollController,
                  child: Container(
                    color: Colors.grey[50],
                    height: MediaQuery.of(context).size.height - 135,
                    child: Column(
                      children: <Widget>[
                        Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Container(
                              width: 60,
                              height: 3,
                              color: Colors.blueGrey[300],
                            )),
                        Container(
                          color: Colors.grey[200],
                          padding: EdgeInsets.all(5),
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.all(10),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      "Notice Board",
                                      style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  )),
                              Divider(
                                thickness: 2,
                                height: 5,
                                color: Colors.grey[700],
                              ),
                              Expanded(
                                flex: 1,
                                child: FlatButton(
                                  color: Colors.grey[500],
                                  child: Text(
                                    "CREATE",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onPressed: () {

                                    if(int.parse(userType) == 1){
                                      Timer(Duration(milliseconds: 500), (){
                                        Navigator.push(
                                            context,
                                            ScaleRoute(
                                                page: OwnerPost(
                                                  buildings: building,
                                                  onNoticeUpdate: updateNoticeBoard,
                                                )));
                                      });
                                    }else{
                                      Navigator.push(
                                          context,
                                          ScaleRoute(
                                              page: Post(
                                                onNoticeUpdate: updateNoticeBoard,
                                              )));
                                    }
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          child: Stack(
                            children: <Widget>[
//                                  AppBackground(),
                              notice == null
                                  ? Container()
                                  : Column(
                                children: <Widget>[
                                  Expanded(
                                    child: ListView(
                                      padding: EdgeInsets.only(top: 0, bottom: 20),
                                      children: notice.map((e) {
                                        return ExpansionCard(houseId: houseId, notice: e, onUpdate: updateNoticeBoard,);
                                      }).toList(),
                                      shrinkWrap: true,
                                      controller: _controllers,
                                    ),
                                  ),
                                ],
                              ),
                              !isProgress
                                  ? Container()
                                  : Align(
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Container(
                                    child:
                                    CircularProgressIndicator(
                                      valueColor:
                                      AlwaysStoppedAnimation<
                                          Color>(
                                          Colors.black),
                                    ),
                                    color: Colors.transparent,
                                    height: 20,
                                    width: 20,
                                  ),
                                ),
                                alignment: Alignment.bottomCenter,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
            )
          ],
        ),
      )),
    );
  }
}
