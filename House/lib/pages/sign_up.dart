import 'dart:async';

import 'package:House/main.dart';
import 'package:House/model/registration_body.dart';
import 'package:House/model/user_type.dart';
import 'package:House/pages/profile/profile.dart';
import 'package:House/pages/sign_in.dart';
import 'package:House/plugin/scale_route.dart';
import 'package:House/service/rest_service.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sms_otp_auto_verify/sms_otp_auto_verify.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  RestService get restService => GetIt.I<RestService>();

  final GlobalKey<FormState> _kayState = GlobalKey<FormState>();

  String _name, _phone, _password, _type;
  final _focus = FocusNode();
  Size size;

  UserType userType; // Option 2
  String _selectedLocation; // Option 2

  int _otpCodeLength = 4;
  bool _isLoadingButton = false;
  bool _enableButton = false;
  String _otpCode = "";
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  String _code;
  String signature;

  bool isPressed = false;

  @override
  void initState() {
    super.initState();
    _getSignatureCode();
    getUserType();
  }

  _getSignatureCode() async {
    signature = await SmsRetrieved.getAppSignature();
  }

  _onOtpCallBack(String otpCode, bool isAutofill) {
    if (!mounted) return;
    setState(() {
      this._otpCode = otpCode;
      if (otpCode.length == _otpCodeLength && isAutofill) {
        _enableButton = false;
        _isLoadingButton = true;
        _verifyOtpCode();
      } else if (otpCode.length == _otpCodeLength && !isAutofill) {
        _enableButton = true;
        _isLoadingButton = false;
      } else {
        _enableButton = false;
      }
    });
  }

  _verifyOtpCode() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    var data = await restService.verification(mobile: _phone, otp: _otpCode);
    if (!data.error) {
      if (data.data == "success") {
        Timer(Duration(milliseconds: 2000), () {
          if (!mounted) return;
          setState(() {
            _isLoadingButton = false;
            _enableButton = false;

            Timer(Duration(seconds: 2), () {
              Navigator.push(context, ScaleRoute(page: MyHomePage()));
            });
          });
        });
      } else {
        setState(() {
          _isLoadingButton = false;
          _enableButton = false;
        });
      }
    }
  }

  _onSubmitOtp() {
    if (!mounted) return;
    setState(() {
      _isLoadingButton = !_isLoadingButton;
      _verifyOtpCode();
    });
  }

  getUserType() async {
    var data = await restService.getUserType();
    if (!data.error) {
      if (!mounted) return;
      setState(() {
        userType = data.data;
      });
    }
  }

  onSignUp() async {
    var formState = _kayState.currentState;
    if (formState.validate()) {
      formState.save();
      var body = RegistrationBody(
          userName: _name,
          mobileno: _phone,
          password: _password,
          usertypeid: _type,
          houseid: "0",
          sign: signature);
      var data = await restService.registration(body: body);
      if (!data.error) {
        if (data.data.result == "success") {
          if (!mounted) return;
          final prefs = await SharedPreferences.getInstance();
          await prefs.setString('userId', data.data.userid.toString());
          await prefs.setString("userType", data.data.usertypeid.toString());
          await prefs.setString("username", _name);
          await prefs.setString("phone", _phone);
          setState(() {
            isPressed = true;
            Timer(Duration(seconds: 2), () {
              Navigator.push(context, ScaleRoute(page: ProfileActivity()));
            });
          });
        }
      }
    }
  }

  Widget _setUpButtonChild() {
    if (_isLoadingButton) {
      return Container(
        width: 19,
        height: 19,
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
        ),
      );
    } else {
      return Text(
        "Verify",
        style: TextStyle(color: Colors.white),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
          body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Stack(
          children: <Widget>[
            Positioned(
                right: -50,
                top: -50,
                child: Image.asset("assets/leaf.png", width: 200)),
            isPressed
                ? Container(
                    child: Center(
                        child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Text(
                            "Verification",
                            style: TextStyle(
                                color: Colors.grey,
                                fontSize: 30,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        TextFieldPin(
                          filled: true,
                          filledColor: Colors.blueGrey[200],
                          codeLength: _otpCodeLength,
                          boxSize: 46,
                          filledAfterTextChange: false,
                          textStyle: TextStyle(fontSize: 16),
                          borderStyle: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(5)),
                          onOtpCallback: (code, isAutofill) =>
                              _onOtpCallBack(code, isAutofill),
                        ),
                        SizedBox(
                          height: 32,
                        ),
                        Container(
                          width: double.maxFinite,
                          child: MaterialButton(
                            onPressed: _enableButton ? _onSubmitOtp : null,
                            child: _setUpButtonChild(),
                            color: Colors.blue,
                            disabledColor: Colors.blue[100],
                            padding: EdgeInsets.all(15),
                          ),
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.only(left: 30, right: 30),
                        ),
                      ],
                    )),
                  )
                : Container(
                    padding: EdgeInsets.only(
                        left: 10, top: 10, bottom: 10, right: 10),
                    child: Form(
                        key: _kayState,
                        child: ListView(
                          shrinkWrap: true,
                          children: <Widget>[
                            SizedBox(height: size.height * .2),
                            Center(
                              child: Text(
                                "Registration",
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(height: 15),
                            Container(
                              margin: EdgeInsets.all(20),
                              child: Column(
                                children: <Widget>[
                                  TextFormField(
                                    // ignore: missing_return
                                    validator: (input) {
                                      if (input.isEmpty) {
                                        return "Name is required";
                                      }
                                    },
                                    style: TextStyle(color: Colors.black),
                                    textInputAction: TextInputAction.next,
                                    keyboardType: TextInputType.text,
                                    onSaved: (input) => _name = input,
                                    onFieldSubmitted: (v) {
                                      FocusScope.of(context)
                                          .requestFocus(_focus);
                                    },
                                    cursorColor: Colors.blueGrey[800],
                                    decoration: InputDecoration(
                                      labelText: "Full Name",
                                      labelStyle: TextStyle(
                                          fontSize: 15,
                                          color: Colors.blueGrey[300]),
                                      filled: true,
                                      focusColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                          color: Colors.transparent,
                                        ),
                                      ),
                                      prefixIcon: Icon(Icons.account_circle,
                                          color: Colors.blueGrey[300]),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                          color: Colors.transparent,
                                          width: 2.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  TextFormField(
                                    // ignore: missing_return
                                    validator: (input) {
                                      if (input.isEmpty) {
                                        return "Phone Number is required";
                                      }
                                    },
                                    style: TextStyle(color: Colors.black),
                                    textInputAction: TextInputAction.next,
                                    keyboardType: TextInputType.phone,
                                    onSaved: (input) => _phone = input,
                                    onFieldSubmitted: (v) {
                                      FocusScope.of(context)
                                          .requestFocus(_focus);
                                    },
                                    cursorColor: Colors.blueGrey[800],
                                    decoration: InputDecoration(
                                      labelText: "Phone Number",
                                      labelStyle: TextStyle(
                                          fontSize: 15,
                                          color: Colors.blueGrey[300]),
                                      filled: true,
                                      focusColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                          color: Colors.transparent,
                                        ),
                                      ),
                                      prefixIcon: Icon(Icons.phone_in_talk,
                                          color: Colors.blueGrey[300]),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                          color: Colors.transparent,
                                          width: 2.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  TextFormField(
                                    // ignore: missing_return
                                    validator: (input) {
                                      if (input.isEmpty || input.length < 6) {
                                        return "Password can't be less then 6 disigt";
                                      }
                                    },
                                    style: TextStyle(color: Colors.black),
                                    focusNode: _focus,
                                    textInputAction: TextInputAction.none,
                                    keyboardType: TextInputType.text,
                                    obscureText: true,
                                    onSaved: (input) => _password = input,
                                    cursorColor: Colors.blueGrey[800],
                                    decoration: InputDecoration(
                                      labelText: "Password",
                                      labelStyle: TextStyle(
                                          fontSize: 15,
                                          color: Colors.blueGrey[300]),
                                      filled: true,
                                      focusColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                          color: Colors.transparent,
                                        ),
                                      ),
                                      prefixIcon: Icon(
                                          Icons.screen_lock_portrait,
                                          color: Colors.blueGrey[300]),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                          color: Colors.transparent,
                                          width: 2.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Container(
                                    color: Colors.grey[200],
                                    padding: EdgeInsets.fromLTRB(20, 5, 5, 5),
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          child: Icon(Icons.merge_type,
                                              color: Colors.blueGrey[300]),
                                          padding: EdgeInsets.only(
                                              top: 12, bottom: 12),
                                        ),
                                        userType == null
                                            ? Container()
                                            : Expanded(
                                                child: Container(
                                                  padding:
                                                      EdgeInsets.only(left: 10),
                                                  child:
                                                      DropdownButtonHideUnderline(
                                                    child: DropdownButton(
                                                      hint: Text(
                                                        'Type',
                                                        style: TextStyle(
                                                            fontSize: 15,
                                                            color: Colors
                                                                .blueGrey[300]),
                                                      ),
                                                      value: _selectedLocation,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyText1,
                                                      onChanged: (newValue) {
                                                        if (!mounted) return;
                                                        setState(() {
                                                          FocusScope.of(context)
                                                              .requestFocus(
                                                                  FocusNode());
                                                          _selectedLocation =
                                                              newValue;
                                                          var id = userType.result
                                                              .firstWhere((element) =>
                                                                  element.value
                                                                      .toLowerCase() ==
                                                                  newValue
                                                                      .toString()
                                                                      .toLowerCase())
                                                              .key;
                                                          _type = id.toString();
                                                        });
                                                      },
                                                      isExpanded: true,
                                                      items: userType.result
                                                          .map((f) {
                                                        return DropdownMenuItem(
                                                          child: new Text(
                                                            f.value,
                                                            style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 15,
                                                            ),
                                                          ),
                                                          value: f.value,
                                                        );
                                                      }).toList(),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  GestureDetector(
                                    onTap: () {
                                      onSignUp();
                                    },
                                    child: Container(
                                      width: 120,
                                      decoration: BoxDecoration(
                                          color: Colors.green[800],
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.green.withOpacity(0.5),
                                              spreadRadius: 1,
                                              blurRadius: 4,
                                              offset: Offset(0, 3),
                                            )
                                          ]),
                                      padding: EdgeInsets.all(10),
                                      child: Center(
                                          child: Text(
                                        "SIGN UP",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      )),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 40,
                                  ),
                                  isPressed
                                      ? Container()
                                      : Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                top: 20,
                                                bottom: 20,
                                                right: 20,
                                                left: 20),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Text(
                                                  "Already have an account?",
                                                  style: TextStyle(
                                                      color:
                                                          Colors.blueGrey[400],
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                GestureDetector(
                                                  onTap: () {
                                                    Navigator.push(
                                                        context,
                                                        ScaleRoute(
                                                            page: SignIn()));
                                                  },
                                                  child: Text(
                                                    "Sign in",
                                                    style: TextStyle(
                                                        fontSize: 15,
                                                        color: Colors.grey,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        )
                                ],
                              ),
                            ),
                          ],
                        )),
                  ),
          ],
        ),
      )),
    );
  }
}
